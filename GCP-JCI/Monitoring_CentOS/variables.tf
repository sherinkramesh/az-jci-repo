#################### Monitoring Module #######################

variable "monitoring_http_project_id" {
  default = ""
}

variable "monitoring_project_id" {
  default = ""
}

variable "monitoring_http_display_name" {
  default = ""
}

variable "monitoring_http_timeout" {
  default = ""
}

variable "monitoring_http_period" {
  default = ""
}
variable "monitoring_http_selected_regions" {
  default = []
}  

variable "monitoring_content_matchers" {
  default = ""
}  

variable "monitoring_http_port" {
  default = ""
}

variable "monitoring_http_path" {
  default = ""
}

variable "monitoring_http_resource_type" {
  default = ""
}

variable "monitoring_tcp_project_id" {
  default = ""
}

variable "monitoring_tcp_display_name" {
  default = ""
}

variable "monitoring_tcp_timeout" {
  default = ""
}

variable "monitoring_tcp_period" {
  default = ""
}
variable "monitoring_tcp_selected_regions" {
  default = []
}  

variable  "monitoring_tcp_port" {
  default = ""
}

variable "monitoring_tcp_resource_type" {
  default = ""
}

variable "monitoring_uptime_filter" {
  default = ""
}

variable "monitoring_uptime_display_name" {
  default = ""
}



