resource "google_monitoring_uptime_check_config" "tcp" {
  project          = var.project_id
  display_name     = var.display_name
  timeout          = var.timeout
  period           = var.period
  selected_regions = var.selected_regions

  tcp_check {
    port = var.tcp_port
  }

  resource_group {
    resource_type = var.tcp_resource_type
    group_id      = google_monitoring_group.check.name
  }
}

resource "google_monitoring_group" "check" {
  display_name = var.uptime_display_name
  filter       = var.uptime_filter
}