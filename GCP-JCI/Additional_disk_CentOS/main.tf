provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}

resource "google_compute_disk" "disk" {
  name        = var.disk_names
  description = var.disk_description
  size        = var.disk_sizes
  type        = var.disk_type
  zone        = var.disk_zones 
  project     = var.project_id
  labels = {
        application_name     = var.disk_label1_value
        business_unit        = var.disk_label2_value
        cost_center          = var.disk_label3_value
        env                  = var.disk_label4_value
        owner                = var.disk_label5_value
        requestor            = var.disk_label6_value
        service_class        = var.disk_label7_value
        troux_id             = var.disk_label8_value
      }
}


resource "google_compute_attached_disk" "disk_attachment" {
  disk         = google_compute_disk.disk.id
  instance     = var.instance_name
  zone         = var.additional_disk_zone
  project      = var.additional_disk_project_id
}

resource "google_compute_disk_resource_policy_attachment" "policy_attachment" {
  name = google_compute_resource_policy.disk-policy.name
  disk = google_compute_disk.disk.name
  zone = var.additional_disk_policy_zone
}


resource "google_compute_resource_policy" "disk-policy" {
  name = var.additional_disk_policy_name
  region = var.additional_disk_policy_region
  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = var.additional_disk_hourly_schedule
        start_time = var.additional_disk_start_time
      }
    }
    retention_policy {
      max_retention_days    = var.additional_disk_retention_days
      on_source_disk_delete = var.additional_disk_delete_policy
    }
    snapshot_properties {
      labels = {
        application_name     = var.policy_label1_value
        business_unit        = var.policy_label2_value
        cost_center          = var.policy_label3_value
        env                  = var.policy_label4_value
        owner                = var.policy_label5_value
        requestor            = var.policy_label6_value
        service_class        = var.policy_label7_value
        troux_id             = var.policy_label8_value
      }
    }
  }
}  

