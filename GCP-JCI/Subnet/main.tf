provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}
/******************************************
	Subnet configuration
 *****************************************/

resource "google_compute_subnetwork" "subnetwork"{
  name                     = var.subnet_name
  ip_cidr_range            = var.subnet_range
  region                   = var.subnet_region
  network                  = var.network_name
  project                  = var.project_id
}




