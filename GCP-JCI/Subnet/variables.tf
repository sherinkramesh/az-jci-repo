variable "project_id" {
  description = "The ID of the project where this VPC will be created"
  default = ""
}

variable "network_name" {
  description = "The name of the network being created"
  default = ""
}

variable "subnet_name"{
  description = "Subent name"
  default = ""
}
variable "subnet_range"{
  description = "Subnet range"
  default = ""
}
variable "subnet_region"{
  description ="Subent region"
  default = ""
}

