output "self_link"{
  description = "Subnet self link"
  value = google_compute_subnetwork.subnetwork.self_link
}
output "Subnet_Name"{
  description = "Subnet self link"
  value = var.subnet_name
}
