#################### Monitoring Module #######################

monitoring_http_project_id= "jci-prj-infra"

monitoring_project_id = "jci-prj-infra"

monitoring_http_display_name = "JCI Windows HTTP Monitoring"

monitoring_http_timeout= "60s"

monitoring_http_period = "60s"

monitoring_http_selected_regions = []

monitoring_content_matchers = "IIS"

monitoring_http_port = "80"

monitoring_http_path = "/"

monitoring_http_resource_type= "uptime_url"

monitoring_tcp_project_id= "jci-prj-infra"

monitoring_tcp_display_name= "JCI Windows TCP Monitoring"

monitoring_tcp_timeout= "60s"

monitoring_tcp_period= "300s"

monitoring_tcp_selected_regions= []

monitoring_tcp_port = "443"

monitoring_tcp_resource_type= "INSTANCE"

monitoring_uptime_filter = "resource.metadata.name=has_substring(\"foo\")"

monitoring_uptime_display_name= "Monitoring-Uptime-check"
