resource "google_monitoring_uptime_check_config" "http" {
  project          = var.project_id
  display_name     = var.display_name
  timeout          = var.timeout
  period           = var.period
  selected_regions = var.selected_regions

 content_matchers {
    content = var.content_matchers
  }
  http_check {
    port         = var.http_port
    path         = var.http_path
   
  }
 monitored_resource {
    type = var.resource_type
    labels = {
      project_id = var.monitoring_project_id
      host         = data.google_compute_address.static-ip.address
    }
  }
}
data "google_compute_address" "static-ip" {
  name = "jci-gcp-windows-ip-address"
}
