variable "project_id" {
  description = "The ID of the project in which the resource belongs"
}

variable "is_http_check" {
  description = "Set it to true if check is http port"
  default     = true
}

variable "display_name" {
  description = "A human-friendly name for the uptime check configuration. The display name should be unique within a Stackdriver Workspace"
}

variable "resource_type" {
  type   = "string"
  default = ""
}

variable "timeout" {
  description = "The maximum amount of time to wait for the request to complete"
  default     = ""
}

variable "period" {
  description = "How often, in seconds, the uptime check is performed. Currently, the only supported values are 60s (1 minute), 300s (5 minutes), 600s (10 minutes), and 900s (15 minutes)"
  default     = ""
}

variable "content_matchers" {
  description = "The expected content on the page the check is run against. Currently, only the first entry in the list is supported, and other entries will be ignored. Supported values are given below"
  type  = "string"
  default     = ""
}

variable "selected_regions" {
  type        = "list"
  description = "The list of regions from which the check will be run. a minimum of 3 locations must be provided. Not specifying this field will result in uptime checks running from all regions"
  default     = []
}

variable "http_port" {
  description = "The port to the page to run the check against.(specif(defaults to 80 without SSL, or 443 with SSL)"
}

variable "monitoring_project_id" {
  default = ""
}

variable "http_path" {
  description = "Http path"
}

variable "use_ssl" {
  description = "If true, use HTTPS instead of HTTP to run the check"
  default     = ""
}

variable "mask_headers" {
  description = "Boolean specifiying whether to encrypt the header information."
  default     = ""
}


variable "is_internal" {
  description = "If this is true, then checks are made only from the 'internal_checkers'. If it is false, then checks are made only from the 'selected_regions'"
  default     = false
}
/*
variable "internal_checkers" {
  type        = "list"
  description = "The internal checkers that this check will egress from. Structure is documented below."
  default     = []
}
/*
/*
gcp_zone - (Optional) The GCP zone the uptime check should egress from. Only respected for internal uptime checks, where internal_network is specified.
peer_project_id - (Optional) The GCP project_id where the internal checker lives. Not necessary the same as the workspace project.
name - (Optional) A unique resource name for this InternalChecker. The format is projects/[PROJECT_ID]/internalCheckers/[INTERNAL_CHECKER_ID]. PROJECT_ID is the stackdriver workspace project for the uptime check config associated with the internal checker.
network - (Optional) The GCP VPC network (https://cloud.google.com/vpc/docs/vpc) where the internal resource lives (ex: "default").
display_name - (Optional) The checker's human-readable name. The display name should be unique within a Stackdriver Workspace in order to make it easier to identify; however, uniqueness is not enforced.
*/
/*
variable "monitored_resource" {
  type        = "list"
  description = "The monitored resource (https://cloud.google.com/monitoring/api/resources) associated with the configuration. Structure is documented below"
  default     = []
}

*/

/*
type - (Required) The monitored resource type. This field must match the type field of a MonitoredResourceDescriptor (https://cloud.google.com/monitoring/api/ref_v3/rest/v3/projects.monitoredResourceDescriptors#MonitoredResourceDescriptor) object. For example, the type of a Compute Engine VM instance is gce_instance. For a list of types, see Monitoring resource types (https://cloud.google.com/monitoring/api/resources) and Logging resource types (https://cloud.google.com/logging/docs/api/v2/resource-list).
labels - (Required) Values for all of the labels listed in the associated monitored resource descriptor. For example, Compute Engine VM instances use the labels "project_id", "instance_id", and "zone".
*/

