provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}

module "jci_monitoring_http" {

  source                     = ("./http")
  project_id                 = var.monitoring_http_project_id
  display_name               = var.monitoring_http_display_name
  timeout                    = var.monitoring_http_timeout
  period                     = var.monitoring_http_period
  selected_regions           = var.monitoring_http_selected_regions
  resource_type              = var.monitoring_http_resource_type
  content_matchers           = var.monitoring_content_matchers
  http_port                  = var.monitoring_http_port
  http_path                  = var.monitoring_http_path
  monitoring_project_id      = var.monitoring_project_id
}

module "jci_monitoring_tcp" {

  source                     = ("./tcp")
  project_id                 = var.monitoring_tcp_project_id
  display_name               = var.monitoring_tcp_display_name
  timeout                    = var.monitoring_tcp_timeout
  period                     = var.monitoring_tcp_period
  selected_regions           = var.monitoring_tcp_selected_regions
  tcp_port                   = var.monitoring_tcp_port
  tcp_resource_type          = var.monitoring_tcp_resource_type
  uptime_filter              = var.monitoring_uptime_filter
  uptime_display_name        = var.monitoring_uptime_display_name
}