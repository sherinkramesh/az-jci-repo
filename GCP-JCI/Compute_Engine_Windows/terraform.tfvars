#################### Compute Module #######################

instance_name = "vm-gcp-windows-jci-prj-infra-webserver-0001"

machine_type = "n1-standard-4"

zone = "europe-west2-a"

network = "gcp-sh-vpc"

subnetwork = "snt-infra-europe-west2-jenkins"

boot_disk_size = "50"

image = "windows-cloud/windows-2016"   

project_id = "jci-prj-infra"

tags = ["allow-http", "allow-rdp", "allow-winrm"]

on_host_maintenance_policy = "MIGRATE"

#################### Snapshot Policy #########################

boot_disk_policy_name = "windows-boot-disk-policy"

boot_disk_policy_region = "europe-west2"

boot_disk_policy_zone = "europe-west2-a"

boot_disk_hourly_schedule = 12

boot_disk_start_time = "23:00"

boot_disk_retention_days = 15

boot_disk_delete_policy = "KEEP_AUTO_SNAPSHOTS"

#################################### GCP Labels ####################

label1_value = ""

label2_value = ""

label3_value = ""

label4_value = ""

label5_value = ""

label6_value = ""

label7_value = ""

label8_value = ""

########################################################################################################




