provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}

module "vm-gcp-jci-prj-infra-webserver-0001-snapshot" {

  source                     = ("./boot_disk_snapshot")
  snapshot_name              = var.snapshot1_name
  disk_zone                  = var.disk1_zone
  project_id                 = var.project_id
  label1_value               = var.gcp_label1_value
  label2_value               = var.gcp_label2_value
  label3_value               = var.gcp_label3_value
  label4_value               = var.gcp_label4_value
  label5_value               = var.gcp_label5_value
  label6_value               = var.gcp_label6_value
  label7_value               = var.gcp_label7_value
  label8_value               = var.gcp_label8_value
}
module "jci-gcp-windows-additional-disk-snapshot" {
  
  source                     = ("./additional_disk_snapshot")
  snapshot_name              = var.snapshot2_name
  disk_zone                  = var.disk2_zone
  project_id                 = var.project_id
  label1_value               = var.gcp_label1_value
  label2_value               = var.gcp_label2_value
  label3_value               = var.gcp_label3_value
  label4_value               = var.gcp_label4_value
  label5_value               = var.gcp_label5_value
  label6_value               = var.gcp_label6_value
  label7_value               = var.gcp_label7_value
  label8_value               = var.gcp_label8_value
}

