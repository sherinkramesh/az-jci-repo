output "snapshot_id" {
  description = "ID of the snapshot created"
  value       = google_compute_snapshot.additional-disk-snapshot.snapshot_id
}

output "self_link" {
  description = "The URI of the created resource"
  value       = google_compute_snapshot.additional-disk-snapshot.self_link
}

output "disk_size_gb" {
  description = "Size of the snapshot, specified in GB"
  value       = google_compute_snapshot.additional-disk-snapshot.disk_size_gb
}
