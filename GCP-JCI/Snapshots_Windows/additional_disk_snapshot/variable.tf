#############################################################
variable "label1_value" {
    default = {}
}
variable "label2_value" {
    default = {}
}
variable "label3_value" {
    default = {}
}
variable "label4_value" {
    default = {}
}
variable "label5_value" {
    default = {}
}
variable "label6_value" {
    default = {}
}
variable "label7_value" {
    default = {}
}
variable "label8_value" {
    default = {}
}

###############################################################################################################
variable "snapshot_name" {
  description = "Name of the resource; provided by the client when the resource is created"
}

variable "disk_zone" {
  description = "A reference to the zone where the disk is hosted"
  default     = ""
}

variable "source_disk" {
  description = "Source disk of Snapshot"
  default = ""
}

variable "snapshot_description" {
  description = "An optional description of this resource"
  default     = ""
}

variable "project_id" {
  default = ""
}

