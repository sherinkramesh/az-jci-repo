variable "firewall_name" {
  description = "Name of the Firewall rule"
}

variable "network" {
  description = "The name or self_link of the network to attach this firewall to"
}

variable "project_id" {
  description = "Project ID of network"
}

variable "target_tags" {
  description = "A list of target tags for this firewall"
}

variable "protocol" {
  description = "The name of the protocol to allow. This value can either be one of the following well known protocol strings (tcp, udp, icmp, esp, ah, sctp), or the IP protocol number, or all"
}

variable "ports" {
  description = "List of ports and/or port ranges to allow. This can only be specified if the protocol is TCP or UDP"
}

variable "source_ip" {
  type = list
  default = []
}