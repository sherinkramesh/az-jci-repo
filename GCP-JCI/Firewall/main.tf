provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}
module "fw-infra-europe-west2-001" {
  source                     = ("./Firewall Module")
  firewall_name              = var.fw1_name
  network                    = var.firewall1_network_name
  protocol                   = var.fw1_protocol
  ports                      = var.fw1_ports
  project_id                 = var.fw1_project_id
  target_tags                = var.fw1_tags
  source_ip                  = var.source_ip_range1
}

module "fw-infra-europe-west2-002" {
  source                     = ("./Firewall Module")
  firewall_name              = var.fw2_name
  network                    = var.firewall2_network_name
  protocol                   = var.fw2_protocol
  ports                      = var.fw2_ports
  project_id                 = var.fw2_project_id
  target_tags                = var.fw2_tags   
  source_ip                  = var.source_ip_range2 
}

module "fw-infra-europe-west2-003" {
  source                     = ("./Firewall Module")
  firewall_name              = var.fw3_name
  network                    = var.firewall3_network_name
  protocol                   = var.fw3_protocol
  ports                      = var.fw3_ports
  project_id                 = var.fw3_project_id
  target_tags                = var.fw3_tags
  source_ip                  = var.source_ip_range3
}

module "fw-infra-europe-west2-004" {
  source                     = ("./Firewall Module")
  firewall_name              = var.fw4_name
  network                    = var.firewall4_network_name
  protocol                   = var.fw4_protocol
  ports                      = var.fw4_ports
  project_id                 = var.fw4_project_id
  target_tags                = var.fw4_tags
  source_ip                  = var.source_ip_range4
}

module "fw-infra-europe-west2-005" {
  source                     = ("./Firewall Module")
  firewall_name              = var.fw5_name
  network                    = var.firewall5_network_name
  protocol                   = var.fw5_protocol
  ports                      = var.fw5_ports
  project_id                 = var.fw5_project_id
  target_tags                = var.fw5_tags
  source_ip                  = var.source_ip_range5
}

module "fw-infra-europe-west2-006" {
  source                     = ("./Firewall Module")
  firewall_name              = var.fw6_name
  network                    = var.firewall6_network_name
  protocol                   = var.fw6_protocol
  ports                      = var.fw6_ports
  project_id                 = var.fw6_project_id
  target_tags                = var.fw6_tags
  source_ip                  = var.source_ip_range6
}