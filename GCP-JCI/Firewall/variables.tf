#################### Firewall Module ########################

variable "fw1_name" {
  default = ""
}

variable "firewall1_network_name" {
  default = ""
}

variable "fw1_protocol" {
  default = ""
}

variable "fw1_ports" {
  default = []
}


variable "fw1_project_id" {
  default = ""
}

variable "fw1_tags" {
  default = []
}

variable "source_ip_range1" {
  default = []
}
#################### Firewal2 Module ########################

variable "fw2_name" {
  default = ""
}

variable "firewall2_network_name" {
  default = ""
}

variable "fw2_protocol" {
  default = ""
}

variable "fw2_ports" {
  default = []
}

variable "fw2_project_id" {
  default = ""
}

variable "fw2_tags" {
  default = []
}

variable "source_ip_range2" {
  default = []
}
#################### Firewal3 Module ########################

variable "fw3_name" {
  default = ""
}

variable "firewall3_network_name" {
  default = ""
}

variable "fw3_protocol" {
  default = ""
}

variable "fw3_ports" {
  default = []
}

variable "fw3_project_id" {
  default = ""
}

variable "fw3_tags" {
  default = []
}

variable "source_ip_range3" {
  default = []
}

#################### Firewal4 Module ########################

variable "fw4_name" {
  default = ""
}

variable "firewall4_network_name" {
  default = ""
}

variable "fw4_protocol" {
  default = ""
}

variable "fw4_ports" {
  default = []
}

variable "fw4_project_id" {
  default = ""
}

variable "fw4_tags" {
  default = []
}
variable "network_tags" {
  default = [] 
}

variable "source_ip_range4" {
  default = []
}

#################### Firewal5 Module ########################

variable "fw5_name" {
  default = ""
}

variable "firewall5_network_name" {
  default = ""
}

variable "fw5_protocol" {
  default = ""
}

variable "fw5_ports" {
  default = []
}

variable "fw5_project_id" {
  default = ""
}

variable "fw5_tags" {
  default = []
}

variable "source_ip_range5" {
  default = []
}

#################### Firewal6 Module ########################

variable "fw6_name" {
  default = ""
}

variable "firewall6_network_name" {
  default = ""
}

variable "fw6_protocol" {
  default = ""
}

variable "fw6_ports" {
  default = []
}

variable "fw6_project_id" {
  default = ""
}

variable "fw6_tags" {
  default = []
}

variable "source_ip_range6" {
  default = []
}
