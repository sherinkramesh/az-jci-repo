#################### Firewall Module ########################

fw1_name = "allow-http"

firewall1_network_name = "gcp-sh-vpc"

fw1_protocol = "tcp"

fw1_ports = [80]

fw1_project_id = "jci-prj-infra"

fw1_tags = ["allow-http"]

source_ip_range1 = ["0.0.0.0/0"]

#################### Firewal2 Module ########################

fw2_name = "allow-https"

firewall2_network_name = "gcp-sh-vpc"

fw2_protocol = "tcp"

fw2_ports = [443]

fw2_project_id = "jci-prj-infra"

fw2_tags = ["allow-https"]

source_ip_range2 = ["0.0.0.0/0"]

#################### Firewal3 Module ########################

fw3_name = "allow-ssh"

firewall3_network_name = "gcp-sh-vpc"

fw3_protocol = "tcp"

fw3_ports = [22]

fw3_project_id = "jci-prj-infra"

fw3_tags = ["allow-ssh"]

source_ip_range3 = ["0.0.0.0/0"]

#################### Firewal4 Module ########################

fw4_name = "allow-apache"

firewall4_network_name = "gcp-sh-vpc"

fw4_protocol= "tcp"

fw4_ports = [80]

fw4_project_id = "jci-prj-infra"

fw4_tags = ["allow-apache"]

network_tags = ["allow-http", "allow-https", "allow-ssh", "allow-apache", "allow-rdp", "allow-winrm"] 

source_ip_range4 = ["0.0.0.0/0"]

#################### Firewal5 Module ########################

fw5_name = "allow-rdp"

firewall5_network_name = "gcp-sh-vpc"

fw5_protocol = "tcp"

fw5_ports = [3389]

fw5_project_id = "jci-prj-infra"

fw5_tags = ["allow-rdp"]

source_ip_range5 = ["0.0.0.0/0"]

#################### Firewal6 Module ########################

fw6_name = "allow-winrm"

firewall6_network_name = "gcp-sh-vpc"

fw6_protocol = "tcp"

fw6_ports = [5985,5986]

fw6_project_id = "jci-prj-infra"

fw6_tags = ["allow-winrm"]

source_ip_range6 = ["0.0.0.0/0"]

