provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}
resource "google_compute_instance" "compute" {
  name                      = var.instance_name
  machine_type              = var.machine_type
  zone                      = var.zone

   network_interface {
    network                 = var.network
    subnetwork              = var.subnetwork
    subnetwork_project      = var.subnetwork_project_id
    access_config {
        nat_ip= google_compute_address.static_ip.address
      }
    }
  allow_stopping_for_update = var.allow_stopping_for_update

  boot_disk {
    auto_delete             = var.auto_delete  
    device_name             = var.device_name
  
   initialize_params {
      size                  = var.boot_disk_size
      type                  = var.boot_disk_type
      image                 = var.image
    }
  }

  lifecycle {
    ignore_changes          = [attached_disk]
  }
  scheduling {
    on_host_maintenance     = var.on_host_maintenance_policy
  }
  can_ip_forward            = var.can_ip_forward
  deletion_protection       = var.deletion_protection
  description               = var.description
  hostname                  = var.hostname
  labels                    = {
        application_name     = var.label1_value
        business_unit        = var.label2_value
        cost_center          = var.label3_value
        env                  = var.label4_value
        owner                = var.label5_value
        requestor            = var.label6_value
        service_class        = var.label7_value
        troux_id             = var.label8_value
  } 
  project                   = var.project_id
  tags                      = var.tags
  metadata = {
    ssh-keys                = "gcpuser:${file("gcpsshkey.pub")}"      
  }
 
 provisioner "file" {
    
        source      = "./boot.sh"
        destination = "/var/tmp/boot.sh"   
        connection {
            type = "ssh"
            host =  "${google_compute_instance.compute.network_interface.0.access_config.0.nat_ip}" 
            user = "gcpuser"
            private_key = file("./gcpsshkey")
        } 
}
   
provisioner "remote-exec" {
        inline = [
            "chmod +x /var/tmp/boot.sh",
            "/var/tmp/boot.sh args",
        ]
        connection {
            type = "ssh"
            host = "${google_compute_instance.compute.network_interface.0.access_config.0.nat_ip}" 
            user = "gcpuser"
            private_key = file("./gcpsshkey")
        }
}    
}

resource "google_compute_address" "static_ip" {
  name = "jci-gcp-centos-ip-address"
}

resource "google_compute_disk_resource_policy_attachment" "policy_attachment" {
  name = google_compute_resource_policy.boot-disk-policy.name
  disk = google_compute_instance.compute.name
  zone = var.boot_disk_policy_zone
}


resource "google_compute_resource_policy" "boot-disk-policy" {
  name = var.boot_disk_policy_name
  region = var.boot_disk_policy_region
  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = var.boot_disk_hourly_schedule
        start_time = var.boot_disk_start_time
      }
    }
    retention_policy {
      max_retention_days    = var.boot_disk_retention_days
      on_source_disk_delete = var.boot_disk_delete_policy
    }
    snapshot_properties {
      labels = {
        application_name     = var.label1_value
        business_unit        = var.label2_value
        cost_center          = var.label3_value
        env                  = var.label4_value
        owner                = var.label5_value
        requestor            = var.label6_value
        service_class        = var.label7_value
        troux_id             = var.label8_value
      }
   }
}
}

