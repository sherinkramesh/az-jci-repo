variable "instance_name" {
  description = "A unique name for the resource, required by GCE"
}

variable "machine_type" {
  description = "The machine type to create"
}

variable "zone" {
  description = "The zone that the machine should be created in"
}

variable "network" {
  description = "The name or self_link of the network to attach this interface to. Either network or subnetwork must be provided"
  default     = ""
}

variable "subnetwork" {
  description = "The name or self_link of the subnetwork to attach this interface to. The subnetwork must exist in the same region this instance will be created in"
  default     = ""
}

variable "subnetwork_project_id" {
  description = "The project in which the subnetwork belongs. If the subnetwork is a self_link, this field is ignored in favor of the project defined in the subnetwork self_link. If the subnetwork is a name and this field is not provided, the provider project is used"
  default     = ""
}

variable "network_ip" {
  description = "The private IP address to assign to the instance. If empty, the address will be automatically assigned"
  default     = ""
}

variable "alias_ip_range" {
  type        = "list"
  description = "An array of alias IP ranges for this network interface. Can only be specified for network interfaces on subnet-mode networks. check terraform doc for supported values"
  default     = []
}

variable "access_config" {
  type        = "list"
  description = "Access configurations, i.e. IPs via which this instance can be accessed via the Internet.Supported values are listed below "
  default     = []
}


variable "allow_stopping_for_update" {
  description = "If true, allows Terraform to stop the instance to update its properties."
  default     = true
}

variable "auto_delete" {
  description = "Whether the disk will be auto-deleted when the instance is deleted"
  default     = true
}

variable "device_name" {
  description = "Name with which attached disk will be accessible. On the instance, this device will be /dev/disk/by-id/google-{{device_name}}"
  default     = ""
}

variable "boot_disk_size" {
  description = "The size of the image in gigabytes. If not specified, it will inherit the size of its base image"
  default     = ""
}

variable "boot_disk_type" {
  description = "The GCE disk type. May be set to pd-standard or pd-ssd"
  default     = "pd-standard"
}

variable "image" {
  description = "The image from which to initialize this disk. This can be one of: the image's self_link, projects/{project}/global/images/{image}, projects/{project}/global/images/family/{family}, global/images/{image}, global/images/family/{family}, family/{family}, {project}/{family}, {project}/{image}, {family}, or {image}."
  default     = ""
}

variable "can_ip_forward" {
  description = "Whether to allow sending and receiving of packets with non-matching source or destination IPs"
  default     = false
}

variable "deletion_protection" {
  description = "Enable deletion protection on this instance"
  default     = false
}

variable "description" {
  description = "A brief description of this resource"
  default     = ""
}

variable "hostname" {
  description = "A custom hostname for the instance. Must be a fully qualified DNS name and RFC-1035-valid"
  default     = ""
}

variable "labels" {
  type        = "map"
  description = "A set of key/value label pairs to assign to the instance"
  default     = {}
}

/*variable "metadata" {
  type        = "map"
  description = "Metadata key/value pairs to make available from within the instance"
  default     = {}
}

variable "metadata_startup_script" {
  description = "An alternative to using the startup-script metadata key, except this one forces the instance to be recreated (thus re-running the script) if it is changed"
  default     = ""
}*/

variable "project_id" {
  description = "The ID of the project in which the resource belongs. If it is not provided, the provider project is used"
  default     = ""
}

variable "service_account" {
  type        = "list"
  description = "Service account to attach to the instance. Refer below for supported values"
  default     = []
}

variable "tags" {
  type        = "list"
  description = "A list of tags to attach to the instance for firewall rules"
  default     = []
}
variable "on_host_maintenance_policy" {
  default = {}
}
variable "label1_value" {
    default = {}
}

variable "label2_value" {
    default = {}
}

variable "label3_value" {
    default = {}
}

variable "label4_value" {
    default = {}
}

variable "label5_value" {
    default = {}
}

variable "label6_value" {
    default = {}
}

variable "label7_value" {
    default = {}
}

variable "label8_value" {
    default = {}
}

#########################################################################################################################################

variable "boot_disk_policy_zone" {
  default = {}
}

variable "boot_disk_policy_name" {
    default = {}
}

variable "boot_disk_policy_region" {
    default = {}
}

variable "boot_disk_hourly_schedule" {
    default = {}
}

variable "boot_disk_start_time" {
    default = {}
}

variable "boot_disk_retention_days" {
    default = {}
}

variable "boot_disk_delete_policy" {
    default = {}
}



##########################################################################################################

