output "instance_id" {
  description = "The server-assigned unique identifier of this instance"
  value       = google_compute_instance.compute.instance_id
}

output "boot_disk_name" {
 value = google_compute_instance.compute.name
}

output "public_ip_address" {
  description = "The IP attached to the instance"
  value       = google_compute_address.static_ip.address
}

