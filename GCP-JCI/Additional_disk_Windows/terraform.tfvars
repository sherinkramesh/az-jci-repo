disk_names = "jci-gcp-windows-additional-disk"

disk_description = "Additional Disk"

disk_sizes = "50"

disk_type = "pd-standard"

disk_zones = "europe-west2-a"

disk_labels = {}

project_id  = "jci-prj-infra"

instance_name = "vm-gcp-windows-jci-prj-infra-webserver-0001"

additional_disk_project_id = "jci-prj-infra"

additional_disk_zone = "europe-west2-a"

disk_label1_value = ""

disk_label2_value = ""

disk_label3_value = ""

disk_label4_value = ""

disk_label5_value = ""

disk_label6_value = ""

disk_label7_value = ""

disk_label8_value = ""

#########################################################################################################################################

additional_disk_policy_zone = "europe-west2-a"

additional_disk_policy_name = "jci-gcp-windows-additional-disk-policy"

additional_disk_policy_region = "europe-west2"

additional_disk_hourly_schedule = 12

additional_disk_start_time = "23:00"

additional_disk_retention_days = 15

additional_disk_delete_policy = "KEEP_AUTO_SNAPSHOTS"

policy_label1_value = ""

policy_label2_value = ""

policy_label3_value = ""

policy_label4_value = ""

policy_label5_value = ""

policy_label6_value = ""

policy_label7_value = ""

policy_label8_value = ""

###############################################################################################################





