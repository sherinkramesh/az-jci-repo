variable "disk_names" {
  description = "Name of the resource"
}

variable "disk_description" {
  description = "An optional description of this resource. Provide this property when you create the resource"
  default     = ""
}

variable "disk_sizes" {
  description = "Size of the persistent disk, specified in GB. You can specify this field when creating a persistent disk using the sourceImage or sourceSnapshot parameter, or specify it alone to create an empty persistent disk"
}

variable "disk_type" {
  description = "URL of the disk type resource describing which disk type to use to create the disk. Provide this when creating the disk"
  default     = "pd-standard"
}

variable "disk_zones" {
  description = "A reference to the zone where the disk resides"
}

variable "disk_labels" {
  description = "Labels to apply to this disk. A list of key->value pairs"
  default     = {}
}

variable "project_id" {
  description = "Project id"
  default       = {}
}

variable "instance_name" {
  description = "Instance ID"
}

variable "additional_disk_project_id" {
  default= ""
}

variable "additional_disk_zone" {
  default =""
}
#############################################################
variable "disk_label1_value" {
    default = {}
}
variable "disk_label2_value" {
    default = {}
}
variable "disk_label3_value" {
    default = {}
}
variable "disk_label4_value" {
    default = {}
}
variable "disk_label5_value" {
    default = {}
}
variable "disk_label6_value" {
    default = {}
}
variable "disk_label7_value" {
    default = {}
}
variable "disk_label8_value" {
    default = {}
}

#############################################################
variable "policy_label1_value" {
    default = {}
}

variable "policy_label2_value" {
    default = {}
}
variable "policy_label3_value" {
    default = {}
}

variable "policy_label4_value" {
    default = {}
}

variable "policy_label5_value" {
    default = {}
}

variable "policy_label6_value" {
    default = {}
}

variable "policy_label7_value" {
    default = {}
}

variable "policy_label8_value" {
    default = {}
}

#########################################################################################################################################

variable "additional_disk_policy_zone" {
  default = {}
}

variable "additional_disk_policy_name" {
    default = ""
}

variable "additional_disk_policy_region" {
    default = ""
}

variable "additional_disk_hourly_schedule" {
    default = ""
}

variable "additional_disk_start_time" {
    default = ""
}

variable "additional_disk_retention_days" {
    default = ""
}

variable "additional_disk_delete_policy" {
    default = ""
}

