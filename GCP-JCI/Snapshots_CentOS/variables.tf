
#################### Snapshot1 Module #######################

variable "snapshot1_name" {
  default = ""
}	

variable "disk1_zone" {
  default = ""
}

#################### Snapshot2 Module #######################

variable "snapshot2_name" {
  default = ""
}

variable "disk2_zone" {
  default = ""
}

variable "project_id" {
  default = ""
}
#################################### GCP Labels ####################

variable "gcp_label1_value" {
    default = ""
}

variable "gcp_label2_value" {
    default = ""
}

variable "gcp_label3_value" {
    default = ""
}

variable "gcp_label4_value" {
    default = ""
}

variable "gcp_label5_value" {
    default = ""
}

variable "gcp_label6_value" {
    default = ""
}

variable "gcp_label7_value" {
    default = ""
}

variable "gcp_label8_value" {
    default = ""
}


