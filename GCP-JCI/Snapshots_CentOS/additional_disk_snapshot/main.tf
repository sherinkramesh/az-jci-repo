
resource "google_compute_snapshot" "additional-disk-snapshot" {
  name        = var.snapshot_name
  source_disk = "jci-gcp-centos-additional-disk"
  zone        = var.disk_zone
  description = var.snapshot_description
  project     = var.project_id
  labels                    = {
        application_name     = var.label1_value
        business_unit        = var.label2_value
        cost_center          = var.label3_value
        env                  = var.label4_value
        owner                = var.label5_value
        requestor            = var.label6_value
        service_class        = var.label7_value
        troux_id             = var.label8_value
  }
}

