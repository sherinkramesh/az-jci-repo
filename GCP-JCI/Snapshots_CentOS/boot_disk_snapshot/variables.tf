##########################################################################################################
variable "snapshot_name" {
  description = "Name of the resource; provided by the client when the resource is created"
}

variable "source_disk" {
  description = "A reference to the disk used to create this snapshot"
  default = ""
}

variable "disk_zone" {
  description = "A reference to the zone where the disk is hosted"
  default     = ""
}

variable "snapshot_description" {
  description = "An optional description of this resource"
  default     = ""
}

variable "project_id" {
  default = ""
}

#######################################################################################################################

variable "label1_value" {
    default = {}
}
variable "label2_value" {
    default = {}
}
variable "label3_value" {
    default = {}
}
variable "label4_value" {
    default = {}
}
variable "label5_value" {
    default = {}
}
variable "label6_value" {
    default = {}
}
variable "label7_value" {
    default = {}
}
variable "label8_value" {
    default = {}
}
