provider "google" {
 credentials = "${file("CREDENTIALS_file.Json")}"
 project     = "jci-prj-infra"
 region      = "europe-west2"
}
resource "google_storage_bucket" "gcp-storage" {

    name = "gcp-storage-europe"
    force_destroy = true
    
    provisioner "local-exec" {
    	command = " gsutil cp ../Subnet/terraform.tfstate gs://jci-gcp-storage/Subnet/terraform.tfstate"
    }
    provisioner "local-exec" {
        command = " gsutil cp ../Firewall/terraform.tfstate gs://jci-gcp-storage/Firewall/terraform.tfstate"
    }    
    provisioner "local-exec" {
        command = " gsutil cp ../Compute_Engine_CentOS/terraform.tfstate gs://jci-gcp-storage/centos/Compute_Engine/terraform.tfstate"
    }
    provisioner "local-exec" {    
        command = " gsutil cp ../Additional_Disk_CentOS/terraform.tfstate gs://jci-gcp-storage/centos/Additional_Disk/terraform.tfstate"
    }
    provisioner "local-exec" {    
        command = " gsutil cp ../Monitoring_CentOS/terraform.tfstate gs://jci-gcp-storage/centos/Monitoring/terraform.tfstate"
    }    
    provisioner "local-exec" {    
        command = " gsutil cp ../Snapshots_CentOS/terraform.tfstate gs://jci-gcp-storage/centos/Snapshots/terraform.tfstate"
    }
    provisioner "local-exec" {
        command = " gsutil cp ../Compute_Engine_Windows/terraform.tfstate gs://jci-gcp-storage/windows/Compute_Engine/terraform.tfstate"
    }
    provisioner "local-exec" {    
        command = " gsutil cp ../Additional_Disk_Windows/terraform.tfstate gs://jci-gcp-storage/windows/Additional_Disk/terraform.tfstate"
    }
    provisioner "local-exec" {    
        command = " gsutil cp ../Monitoring_Windows/terraform.tfstate gs://jci-gcp-storage/windows/Monitoring/terraform.tfstate"
    }    
    provisioner "local-exec" {    
        command = " gsutil cp ../Snapshots_Windows/terraform.tfstate gs://jci-gcp-storage/windows/Snapshots/terraform.tfstate"
    }  
	
}

