data "oci_identity_availability_domains" "GetAds" {
  compartment_id = var.tenancy_ocid
}
data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}

data "oci_core_vcns" "test_vcns" {
    #Required
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = "${var.vcn_display_name}"
    
}
data "oci_core_subnets" "test_subnets" {
    #Required
    compartment_id ="${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    vcn_id = data.oci_core_vcns.test_vcns.virtual_networks[0].id

    #Optional
    display_name = "${var.subnet_display_name}"
    
}
data "oci_objectstorage_object" "bootstrap" {
    #Required
    bucket = "${var.object_bucket}"
    namespace = "${var.object_namespace}"
    object = "${var.object_object}"
}
resource "oci_core_instance" "CreateInstance" {
  availability_domain = lookup(data.oci_identity_availability_domains.GetAds.availability_domains[1], "name")
  compartment_id      = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
  shape               = var.shape_id
  defined_tags=var.tags
  agent_config {

        #Optional
        is_management_disabled = "${var.is_management_disabled}"
        is_monitoring_disabled = "${var.is_monitoring_disabled}"
    }
  source_details {
    source_id   = var.windows_image_id
    source_type = "image"
    boot_volume_size_in_gbs = "${var.instance_source_details_boot_volume_size_in_gbs}"
  
  }
  display_name           = var.instance_display_name
  hostname_label         = var.hostname_label
  fault_domain = "${var.instance_fault_domain}"
  create_vnic_details {
  
    defined_tags = var.tags
    subnet_id              = data.oci_core_subnets.test_subnets.subnets[0].id
    skip_source_dest_check = var.instance_create_vnic_details_skip_source_dest_check
    hostname_label         = var.hostname_label
    assign_public_ip       = var.assign_public_ip
  }
  metadata = {
    user_data = "${base64encode(data.oci_objectstorage_object.bootstrap.content)}"
   
  }
  subnet_id    = data.oci_core_subnets.test_subnets.subnets[0].id
  preserve_boot_volume = true
}


resource "oci_core_volume_backup_policy" "test_volume_backup_policy" {
  compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
  defined_tags = var.tags
  display_name = var.volume_backup_policy_display_name
  schedules {
        backup_type = "${var.volume_backup_policy_schedules_backup_type}"
        period = "${var.volume_backup_policy_schedules_period}"
        offset_seconds = "${var.volume_backup_policy_schedules_offset_seconds}"
        retention_seconds = "${var.volume_backup_policy_schedules_retention_seconds}"
        hour_of_day = "${var.volume_backup_policy_schedules_hour_of_day}"
        offset_type = "${var.volume_backup_policy_schedules_offset_type}"
       }
}
resource "oci_core_volume" "CreateBlockVolume" {
    availability_domain = lookup(data.oci_identity_availability_domains.GetAds.availability_domains[1], "name")
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = var.volume_display_name
    defined_tags = var.tags
    size_in_gbs = var.volume_size_in_gbs
   backup_policy_id = oci_core_volume_backup_policy.test_volume_backup_policy.id
    
       }
  
  resource "oci_core_volume_attachment" "Block_volume_attachment" {
    
    attachment_type = var.volume_attachment_attachment_type
    instance_id = oci_core_instance.CreateInstance.id
    volume_id = oci_core_volume.CreateBlockVolume.id

   
    display_name = var.volume_attachment_display_name
    is_pv_encryption_in_transit_enabled = var.volume_attachment_is_pv_encryption_in_transit_enabled
    is_read_only = var.volume_attachment_is_read_only
    is_shareable = var.volume_attachment_is_shareable
    use_chap = var.volume_attachment_use_chap
}
 

resource "oci_core_volume_backup_policy_assignment" "test_volume_backup_policy_assignment" {
    #Required
    asset_id = oci_core_instance.CreateInstance.boot_volume_id
    policy_id = oci_core_volume_backup_policy.test_volume_backup_policy.id
   
    }

