output "instances" {
  value = oci_core_instance.CreateInstance
}

output "boot_volume_id" {
  value = "${oci_core_instance.CreateInstance.boot_volume_id}"
}


