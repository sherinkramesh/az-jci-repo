data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}

data "oci_core_vcns" "test_vcns" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = "${var.vcn_display_name}"
}
resource "oci_core_network_security_group" "create_network_security_group" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    vcn_id = data.oci_core_vcns.test_vcns.virtual_networks[0].id
    defined_tags = var.tags
    display_name = var.nsg_display_name
}
    