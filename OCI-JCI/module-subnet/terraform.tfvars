# OCI Authentication details
tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaajwms2b2qcpkw6palnkq2bxzoimyfcob73jyrdkabhaq2gmalfmwq"
user_ocid = "ocid1.user.oc1..aaaaaaaaynnuswcvq3zoows3ye455b26ln5ve3wb6dm576nubr3fkoqdpina"
fingerprint = "c4:ec:43:2e:0b:83:e8:5d:64:7c:42:31:4e:b4:ea:e9"
private_key_path = "C:\\Users\\ksherin\\.oci\\oci_api_key.pem"
region = "us-ashburn-1"

compartment_display_name="JCI-COMP-INFA-P"
sub_display_name = "snt-inf-pr-iad-001"
sub_dns_label = "sntinfpriad001"
sub_cidr_block="10.85.0.0/21"
tags={
     "JCI.env" = "it"
    "JCI.BusinessUnit" = "bts"
    "JCI.CostCenter" =  "56"
    "JCI.Owner"="sangeetha"
    "JCI.Requester"="sa"
    "JCI.TrouxID"="019"
    "JCI.ServiceClass"="standard"
    "JCI.ApplicationName"="terraform"
 }
security_list_display_name="sec-inf-pr-iad-001"
vcn_display_name="oci-iad-vcn-infr"
route_table_display_name="rtb-inf-pr-iad-001"

