# Subnet variables
variable "sub_cidr_block" {
  default = "172.27.0.0/24"
}
variable "sub_dns_label" {
  default=""
  
}


variable "compartment_display_name" {
  default=""
}
variable "prohibit_public_ip_on_vnic" { default = "false" }

variable "sub_display_name" {
  default = "app-subnet"
}


variable "dns_label_subnet" {
  default = ""
}
variable "vcn_default_security_list_id" { 
  type = list 
default=["ocid1.securitylist.oc1.iad.aaaaaaaavcnhhck4u5pqvy2wpbnylb266e43xrau45hx4ibq27rth4tlvvqq"]
}
variable "vcn_default_route_table_id" {
  default="ocid1.routetable.oc1.iad.aaaaaaaa2j4mu4gjwgypl4drwtwyjphci2za3y5ycm52bopqijycqoyp5adq"
}
variable "vcn_default_dhcp_options_id" {
  default=""
}

variable "tags" {
  type=map
  default = {
    
  }
}

variable "vcn_display_name" {
  default=""
  
}
variable "security_list_display_name" {
  default=""
  
}


variable "route_table_display_name" {
  default=""
  
}


