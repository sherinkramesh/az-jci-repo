data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}
data "oci_core_vcns" "test_vcns" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = var.vcn_display_name
}

data "oci_core_security_lists" "test_security_lists" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    vcn_id = data.oci_core_vcns.test_vcns.virtual_networks[0].id
    display_name = var.security_list_display_name
    
}
data "oci_identity_availability_domains" "GetAds" {
  compartment_id = var.tenancy_ocid
}

data "oci_core_route_tables" "test_route_tables" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    vcn_id = data.oci_core_vcns.test_vcns.virtual_networks[0].id
    display_name = var.route_table_display_name
   }

resource "oci_core_subnet" "CreateSubnet" {
  availability_domain        = lookup(data.oci_identity_availability_domains.GetAds.availability_domains[1], "name")
  cidr_block                 = var.sub_cidr_block
  defined_tags               = var.tags
  display_name               = var.sub_display_name
  dns_label                  = var.sub_dns_label
  compartment_id             = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
  vcn_id                     = data.oci_core_vcns.test_vcns.virtual_networks[0].id
  security_list_ids          = [data.oci_core_security_lists.test_security_lists.security_lists[0].id]
  route_table_id             = data.oci_core_route_tables.test_route_tables.route_tables[0].id
  dhcp_options_id            = var.vcn_default_dhcp_options_id
  prohibit_public_ip_on_vnic = var.prohibit_public_ip_on_vnic
}
