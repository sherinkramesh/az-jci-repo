# OCI Authentication details
tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaajwms2b2qcpkw6palnkq2bxzoimyfcob73jyrdkabhaq2gmalfmwq"
user_ocid = "ocid1.user.oc1..aaaaaaaaynnuswcvq3zoows3ye455b26ln5ve3wb6dm576nubr3fkoqdpina"
fingerprint = "c4:ec:43:2e:0b:83:e8:5d:64:7c:42:31:4e:b4:ea:e9"
private_key_path = "C:\\Users\\ksherin\\.oci\\oci_api_key.pem"
region = "us-ashburn-1"

compartment_display_name="JCI-COMP-INFA-P"
ingress_ports=[
  { minport     = 22
      maxport     = 22
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 3389
      maxport     = 3389
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 80
      maxport     = 80
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 443
      maxport     = 443
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 1521
      maxport     = 1521
      source_cidr = "0.0.0.0/0"
    },    
    { minport     = 0
      maxport     = 0
      source_cidr = "10.85.0.0/16"
  }]

vcn_display_name="oci-iad-vcn-infr"
sl_display_name="sec-inf-pr-iad-001"
egress_destination="0.0.0.0/0"
egress_protocol="all"
ingress_source="0.0.0.0/0"
ingress_protocol="6"
ingress_stateless=false
 tags={
     "JCI.env" = "it"
    "JCI.BusinessUnit" = "bts"
    "JCI.CostCenter" =  "56"
    "JCI.Owner"="sangeetha"
    "JCI.Requester"="sa"
    "JCI.TrouxID"="019"
    "JCI.ServiceClass"="standard"
    "JCI.ApplicationName"="terraform"
 }