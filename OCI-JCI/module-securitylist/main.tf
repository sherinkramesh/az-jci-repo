data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}

data "oci_core_vcns" "test_vcns" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = var.vcn_display_name
}
resource "oci_core_security_list" "CreateSecurityList" {
  compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
  vcn_id         = data.oci_core_vcns.test_vcns.virtual_networks[0].id
  display_name   = var.sl_display_name
  defined_tags   = var.tags
  egress_security_rules {
    destination = var.egress_destination
    protocol    = var.egress_protocol
  }
  dynamic "ingress_security_rules" {
    iterator = port
    for_each = [for y in var.ingress_ports : {
      minport     = y.minport
      maxport     = y.maxport
      source_cidr = y.source_cidr
    }]
    content {
      protocol  = var.ingress_protocol
      source    = port.value.source_cidr
      stateless = var.ingress_stateless
      tcp_options {
        min = port.value.minport
        max = port.value.maxport
      }
    }
  }
}

