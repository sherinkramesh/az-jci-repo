# Applications security list variables
variable "ingress_ports" {
  description = "all the ports for security list"
  default = [
    { minport     = 22
      maxport     = 22
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 3389
      maxport     = 3389
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 80
      maxport     = 80
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 443
      maxport     = 443
      source_cidr = "0.0.0.0/0"
    },
    { minport     = 0
      maxport     = 0
      source_cidr = "172.27.0.0/16"
  }]
}

variable "compartment_display_name" {
  default=""
}
variable "sl_display_name" {
   default = "MyPrivateSL"
}
variable "vcn_display_name" {
  default=""
  
}
variable "egress_destination" {
  default = "0.0.0.0/0"
}

variable "egress_protocol" {
  default = "all"
}

variable "ingress_source" {
  default = "0.0.0.0/0"
}

variable "ingress_protocol" {
  default = "6"
}

variable "ingress_stateless" {
  default = false
}
variable "tags" {
  type=map
  default = {
    
  }
}