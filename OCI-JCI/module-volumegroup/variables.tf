variable "compartment_display_name" {
    default=""
}
variable "volume_group_availability_domain" {
    default=""
}
variable "volume_group_source_ids" {
    default=""
}
variable "volume_group_display_name" {
    default=""
}
variable "tags" {
    type=map
    default={}
    }
variable "volume_source_type" {
    default=""
}

variable "volume_display_name" {
    default=""
  
}

variable "instance_display_name" {
    default=""
  
}
