data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}
data "oci_core_volumes" "test_volumes" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = "${var.volume_display_name}"
    }
data "oci_core_instances" "test_instances" {
   compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
   display_name = "${var.instance_display_name}"
  }

resource "oci_core_volume_group" "test_volume_group" {
    availability_domain = "${var.volume_group_availability_domain}"
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    source_details {
        type = "volumeIds"
        volume_ids = [data.oci_core_instances.test_instances.instances[0].boot_volume_id, data.oci_core_volumes.test_volumes.volumes[0].id]
    }

    defined_tags = var.tags
    display_name = "${var.volume_group_display_name}"
}