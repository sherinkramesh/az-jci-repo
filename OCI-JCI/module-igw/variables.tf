# IGW variables
variable "internet_gateway_enabled" { default = "true" }
variable "internet_gateway_display_name" { default = "MyIGW" }

variable "igw_route_table_rules_cidr_block" { default = "0.0.0.0/0" }
variable "compartment_display_name" {
  default=""
}
variable "tags"{
    type=map
    default={}
}
variable "vcn_display_name" {
  default=""
}
