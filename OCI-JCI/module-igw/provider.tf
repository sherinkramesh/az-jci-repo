variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "region" {}

provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
  region           = var.region
 }

terraform {
   backend "s3" {
       endpoint = "compat.objectstorage.us-ashburn-1.oraclecloud.com" # This will be always the same
       region = "us-ashburn-1" # or us-ashburn-1
       bucket   = "idqmfqz0kdsv"
       key      = "terraform_tf_states/igw-iad-vcn-inf01_terraform.tfstate" # This should contain "[OCI_BUCKET_NAME]/[OBJECT_NAME]"
       shared_credentials_file="~/.aws/credentials"
       profile                 = "default"
       access_key = "c3624bcef77532c743097ad465fedd7cec1a8fd4" 
       secret_key = "kWjqO9H1hf/8aNyEQecH1hAOjyh93AL3hUh7ARQxgtg="
       # All S3-specific validations are skipped:
       skip_region_validation = true
       skip_credentials_validation = true 
       skip_get_ec2_platforms = true
       skip_requesting_account_id = true
       skip_metadata_api_check = true
   }
}
