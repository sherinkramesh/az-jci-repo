variable "compartment_display_name" {
    default=""
}
variable "instance_availability_domain" {
    default=""
}

variable "tags" {
  type=map
  default = {
    
  }
}
variable "object_bucket" {
  default=""
}
variable "object_namespace" {
  default=""
}
variable "object_object" {
  default=""
}
variable "instance_fault_domain" {
  default=""
}

variable "image_id" {
    default=""
}
variable "subnet_id" {
    default=""
}

variable "operating_system" {
  default = "Oracle Linux"
} // Name for the OS

variable "operating_system_version" {
  default = "7.7"
} // OS Version

variable "shape_id" {
  default = "VM.Standard2.1"
} // Shape what to be used. Smallest shape selected by default

variable "source_type" {
  default = "image"
} // What type the image source is

variable "assign_public_ip" {
  default = "true"
} // This is server in public subnet it will have a public IP

variable "instance_display_name" {
   default = "Linux_Demo"
}
variable "hostname_label"{
  default="demo"
}
variable "instance_create_vnic_details_hostname_label" {
  default = "demo"
}
variable "is_management_disabled" {
  default = false
}
variable "is_monitoring_disabled" {
  default = false
}

variable "instance_launch_options_boot_volume_type" {
  default="ISCSI"
}
variable "instance_source_details_boot_volume_size_in_gbs" {
  default="50"
}
/*variable "ssh_public_key_path" {
  default = "C:\\Users\\sangeetn\\Downloads\\New Folder\\JCI_OCI_scripst\\PublicKey.pub"
}*/
variable "windows_image_id" {
  default="ocid1.image.oc1.iad.aaaaaaaa3kk2nwd7yrqrkz5psvkh7m3deop645q3qkl7npnqebxk224mxs4a"
}
variable "boot_volume_availability_domain" {
  default=""
}
variable "boot_volume_size_in_gbs" {
  default= "60"
}
variable "boot_volume_vpus_per_gb" {
  default="10"
}

variable "instance_create_vnic_details_skip_source_dest_check" {
  default = false
}

variable "volume_backup_policy_display_name" {
    default=""
}
variable "volume_backup_policy_schedules_backup_type" {
    default=""
}

variable "volume_backup_policy_schedules_period" {
    default=""
}
variable "volume_backup_policy_schedules_retention_seconds" {
    default=""
}

variable "volume_backup_policy_schedules_offset_seconds" {
    default=""
  
}
variable "volume_display_name" {
  default=""
}

variable "volume_backup_policy_schedules_hour_of_day" {
  default=""
}
 variable "volume_backup_policy_schedules_offset_type"{
     default=""
 }
 variable "volume_size_in_gbs"{
default="50"
}

variable "volume_attachment_attachment_type"{
default="iscsi"
}

variable "volume_attachment_display_name"{
default="test_volume"
}

variable "volume_attachment_is_pv_encryption_in_transit_enabled"{
default="false"
}

variable "volume_attachment_is_read_only"{
default="false"
}

variable "volume_attachment_is_shareable"{
default="false"
}

variable "volume_attachment_use_chap"{
default="false"
}

variable "subnet_display_name" {
  default=""
  
}

variable "vcn_display_name" {
  default=""
  
}
