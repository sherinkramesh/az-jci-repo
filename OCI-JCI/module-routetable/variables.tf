
variable "route_table_display_name" {
  default = "MyRouteTable"
}

variable "compartment_display_name" {
  default=""
}
variable "route_table_route_rules_cidr_block" {
  default="0.0.0.0/0"
}

variable "internet_gateway_display_name" {
  default=""
}

variable "vcn_display_name" {
  default=""
  
}

variable "tags" {
  type=map
  default = {
    
  }
}
