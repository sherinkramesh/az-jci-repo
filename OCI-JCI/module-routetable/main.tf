data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}
data "oci_core_vcns" "test_vcns" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = var.vcn_display_name
}

data "oci_core_internet_gateways" "test_internet_gateways" {
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    vcn_id = data.oci_core_vcns.test_vcns.virtual_networks[0].id
    display_name = var.internet_gateway_display_name
}

resource "oci_core_route_table" "CreateRouteTable" {
  compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
  vcn_id         = data.oci_core_vcns.test_vcns.virtual_networks[0].id
  defined_tags   = var.tags
  display_name   = var.route_table_display_name
  route_rules {
       network_entity_id = data.oci_core_internet_gateways.test_internet_gateways.gateways[0].id
      destination       = var.route_table_route_rules_cidr_block
  }
}
 