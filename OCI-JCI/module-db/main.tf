data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}
data "oci_objectstorage_bucket" "test_bucket" {
    #Required
    name = "${var.bucket_name}"
    namespace = "${var.bucket_namespace}"
}
data "oci_identity_availability_domains" "GetAds" {
  compartment_id = var.tenancy_ocid
}
data "oci_core_vcns" "test_vcns" {
    #Required
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    display_name = "${var.vcn_display_name}"
    
}
data "oci_core_subnets" "test_subnets" {
    #Required
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    vcn_id = data.oci_core_vcns.test_vcns.virtual_networks[0].id

    #Optional
    display_name = "${var.subnet_display_name}"
    
}

resource "oci_database_db_system" "test_db_system" {
    #Required
    availability_domain = lookup(data.oci_identity_availability_domains.GetAds.availability_domains[1], "name")
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    database_edition = "${var.db_system_database_edition}"
    db_home {
        #Required
        database {
            #Required
            admin_password = "${var.db_system_db_home_database_admin_password}"
            character_set = "${var.db_system_db_home_database_character_set}"
            db_backup_config {

                #Optional
                auto_backup_enabled = "${var.db_system_db_home_database_db_backup_config_auto_backup_enabled}"
                auto_backup_window = "${var.db_system_db_home_database_db_backup_config_auto_backup_window}"
                backup_destination_details {

                    #Optional
                    id = "${data.oci_objectstorage_bucket.test_bucket.bucket_id}"
                    type = "${var.db_system_db_home_database_db_backup_config_backup_destination_details_type}"
                }
                recovery_window_in_days = "${var.db_system_db_home_database_db_backup_config_recovery_window_in_days}"
            }
            db_name = "${var.db_system_db_home_database_db_name}"
            db_workload = "${var.db_system_db_home_database_db_workload}"
            defined_tags = "${var.tags}"
            ncharacter_set = "${var.db_system_db_home_database_ncharacter_set}"
            pdb_name = "${var.db_system_db_home_database_pdb_name}"
        }

        #Optional
        db_version = "${var.db_system_db_home_db_version}"
        display_name = "${var.db_system_db_home_display_name}"
    }
    hostname = "${var.db_system_hostname}"
    shape = "${var.db_system_shape}"
    ssh_public_keys =[file("PublicKey.pub")]
    display_name = "${var.db_system_display_name}"
    subnet_id = data.oci_core_subnets.test_subnets.subnets[0].id
    license_model=var.license_model
    fault_domains = ["${var.db_system_fault_domains}"]
    disk_redundancy=var.disk_redundancy
    node_count = "${var.db_system_node_count}"
    cpu_core_count = "${var.db_system_cpu_core_count}"
    data_storage_size_in_gb = "${var.db_system_data_storage_size_in_gb}"
    
    defined_tags = "${var.tags}"
    
}