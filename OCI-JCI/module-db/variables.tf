variable "compartment_display_name" {
  default=""
}
variable "tags" {
  type=map
  default = {
    
  }
}

variable "db_system_database_edition" {
  default=""
}
variable "db_system_db_home_database_admin_password" {
  default=""
}
variable "db_system_db_home_database_character_set" {
  default=""
}

variable "db_system_db_home_database_db_backup_config_auto_backup_enabled" {
  default=""
}
variable "db_system_db_home_database_db_backup_config_auto_backup_window" {
  default=""
}

variable "bucket_name" {
 default="" 
}
variable "bucket_namespace" {
  default=""
}
variable "db_system_db_home_database_db_backup_config_backup_destination_details_type" {
  default=""
}
variable "db_system_db_home_database_db_backup_config_recovery_window_in_days" {
  default=""
}
variable "db_system_db_home_database_db_name" {
  default=""
  }
variable "db_system_db_home_database_db_workload" {
  default=""
}
variable "db_system_db_home_database_ncharacter_set" {
  default=""
}
variable "db_system_db_home_database_pdb_name" {
  default=""
}
variable "db_system_db_home_db_version" {
  default=""
}
variable "db_system_db_home_display_name" {
  default=""
}
variable "db_system_hostname" {
  default=""
}
variable "db_system_shape" {
  default=""
}

variable "vcn_display_name" {
  default=""
}
variable "subnet_display_name" {
  default=""
}

variable "license_model" {
  default=""
}
variable "disk_redundancy" {
  default=""
}

variable "db_system_node_count" {
  default=""
}

variable "db_system_cpu_core_count" {
  default=""
}
variable "db_system_data_storage_size_in_gb" {
  default=""
}
variable "db_system_display_name" {
  default=""
}
variable "db_system_fault_domains" {
  default=""
}








