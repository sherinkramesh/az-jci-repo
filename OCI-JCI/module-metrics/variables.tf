variable "notification_topic_name" {
    default=""
    }
variable "compartment_display_name" {
    default=""
}
variable "alarm_display_name" {
    default=""
}
variable "alarm_is_enabled"{
    default=""
}
variable "alarm_metric_compartment_id" {
    default=""
}
variable "alarm_namespace" {
    default=""
}
variable "alarm_query" {
    default=""
}
variable "alarm_severity" {
    default=""
}
variable "topic_destinations" {
    type=list
    default=[]
}


variable "subscription_endpoint" {
    default=""
}
variable "subscription_protocol" {
    default=""
}
