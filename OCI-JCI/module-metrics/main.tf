data "oci_identity_compartments" "GetCompartments" {
compartment_id = "${var.tenancy_ocid}"
 filter{
 name="name"
 values= ["${var.compartment_display_name}"]
 }
}

resource "oci_ons_notification_topic" "test_notification_topic"{
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    name = var.notification_topic_name
} 
resource "oci_ons_subscription" "test_subscription"{
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    endpoint = var.subscription_endpoint
    protocol = var.subscription_protocol
    topic_id = oci_ons_notification_topic.test_notification_topic.topic_id
}

resource "oci_monitoring_alarm" "test_alarm"{
    compartment_id = "${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    destinations = [oci_ons_notification_topic.test_notification_topic.topic_id]
    display_name = var.alarm_display_name
    is_enabled = var.alarm_is_enabled
    metric_compartment_id ="${lookup(data.oci_identity_compartments.GetCompartments.compartments[0],"id")}"
    namespace = var.alarm_namespace
    query = var.alarm_query
    severity = var.alarm_severity
   
}
