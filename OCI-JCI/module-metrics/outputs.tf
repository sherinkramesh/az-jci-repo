output "topic" {
  value = oci_ons_notification_topic.test_notification_topic
}

output "id" {
  value = oci_ons_notification_topic.test_notification_topic.topic_id
}