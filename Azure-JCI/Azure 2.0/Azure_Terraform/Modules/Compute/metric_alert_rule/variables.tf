variable "action_group_name" {
  description = "Specifies the name of the alert rule"
 }

variable "resource_group_name" {
  description = "The name of the resource group in which to create the alert rule"
 }
variable "custom_emails" {
  description = "A email addresses to be notified when the alert is triggered"
  type        = string
  default     = ""
 }
variable "metric_rule_name" {
  description = "Specifies the name of the alert rule"
  default= ""
 }
variable "scope" {
  description = "Specifies the name of the alert rule"
  default= ""
 }

variable "alert_description" {
  description = "A verbose description of the alert rule that will be included in the alert email"
 }
variable "metric_name" {
  description = "The metric that defines what the rule monitors. For list of mrtic names, refer https://docs.microsoft.com/en-us/azure/monitoring-and-diagnostics/monitoring-supported-metrics"
 }

variable "metric_operator" {
  description = "The operator used to compare the metric data and the threshold. Possible values are GreaterThan, GreaterThanOrEqual, LessThan, and LessThanOrEqual"
 }

variable "threshold" {
  description = "The threshold value that activates the alert"
 }

variable "aggregation" {
  description = "Defines how the metric data is combined over time. Possible values are Average, Minimum, Maximum, Total, and Last"
  default     = "Average"
 }

variable "tags" {
  description = "Tags for alert rule"
  type        = map
  default     = {}
}

