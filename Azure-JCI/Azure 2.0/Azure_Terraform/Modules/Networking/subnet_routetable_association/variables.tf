variable "subnet_id" {
  description = "Id of the subnet id  to associate with nsg"
  default = ""
}

variable "route_table_id" {
  description = "Id of the route table to associate with subnet"
  default = ""
}
