variable "resource_group_name" {
  description = "Resource group name that the network will be created in"
 }

variable "virtual_network_name" {
  description = "Name of the vnet to assocaite with subnet"
 }

variable "subnet_prefixes" {
  description = "The address prefix to use for the subnet"
  type = list(string)
  default     = []
 }

variable "subnet_names" {
 description = "A list of public subnets inside the vNet"
 type = list(string)
 default     = []
 }
