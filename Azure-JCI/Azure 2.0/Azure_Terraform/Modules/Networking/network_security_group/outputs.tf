output "nsg_id" {
  description = "id of the security group provisioned"
  value       = "${azurerm_network_security_group.nsg.id}"
}

output "nsg_name" {
  description = "Name of the security group provisioned"
  value       = "${var.nsg_name}"
}
