variable "route_table_name" {
  description = "Name of the Route Table"
  default = ""
 }

variable "route_name" {
  description = "Name of the route"
  default = ""
 }

variable "route_address_prefix" {
  description = "The Address space of the route to redirect the trafic"
  default = ""
 }

variable "next_hop_type" {
  description = "The type of Azure hop the packet should be sent to. Possible values are VirtualNetworkGateway, VnetLocal, Internet, VirtualAppliance and None."
  default = "None" 
 }

variable "next_hop_in_ip_address" {
  description = "Contains the IP address packets should be forwarded to. Next hop values are only allowed in routes where the next hop type is VirtualAppliance."
  default = "" 
 }

variable "bgp_route_propagation" {
  description = "The tags to associate with your route table"
  default = true
 }
variable "tags" {
  description = "The tags to associate with your route table"
  type        = map
  default = {}
 }

variable "resource_group_name" {
  description = "Resource group name that the network will be created in"
 }

variable "location" {
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
 }
