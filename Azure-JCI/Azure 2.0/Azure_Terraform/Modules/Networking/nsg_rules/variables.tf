variable "resource_group_name" {
  description = "Name of the resource group name where nsg is available"
 }

variable "nsg_name" {
  description = "Name of the NSG for which we need to add a rule"
  default = ""
 }

variable "rule_name" {
  description = "The name of the security rule. This needs to be unique across all Rules in the Network Security Group"
 }

variable "rule_priority" {
  description = "Specifies the priority of the rule. The value can be between 100 and 4096. The priority number must be unique for each rule in the collection"
 }

variable "rule_direction" {
  description = "The direction specifies if rule will be evaluated on incoming or outgoing traffic. Possible values are Inbound and Outbound"
  type = string
 }

variable "rule_access" {
  description = "Specifies whether network traffic is allowed or denied. Possible values are Allow and Deny"
  default     = "Allow"
 }

variable "rule_protocol" {
  description = "Network protocol this rule applies to. Possible values include Tcp, Udp or * "
  default     = "Tcp"
 }

variable "source_port_range" {
  description = "Source Port or Range. Integer or range between 0 and 65535 or * to match any.This is required if source_port_ranges is not specified"
  default     = "*"
 }

variable "destination_port_range" {
  description = "Destination Port or Range. Integer or range between 0 and 65535 or * to match any. This is required if destination_port_ranges is not specified"
  default     = "*"
 }

variable "source_address_prefix" {
  description = "CIDR or source IP range or * to match any IP. Tags such as ‘VirtualNetwork’, ‘AzureLoadBalancer’ and ‘Internet’ can also be used. This is required if source_address_prefixes is not specified"
  default     = "*"
 }

variable "source_nsg_ids" {
  description = "A List of source Application Security Group ID's"
  type        = list
  default     = []
 }

variable "destination_address_prefix" {
  description = "CIDR or destination IP range or * to match any IP. Tags such as ‘VirtualNetwork’, ‘AzureLoadBalancer’ and ‘Internet’ can also be used. This is required if destination_address_prefixes is not specified"
  default     = "*"
 }

variable "destination_nsg_ids" {
  description = "A List of destination Application Security Group ID's"
  type        = list
  default     = []
 }

variable "rule_description" {
  description = "A description for this rule. Restricted to 140 characters"
  default     = ""
 }
