data "azurerm_resource_group" "resource_group" {
  name = var.resource_group_name
 }

data "azurerm_virtual_network" "virtual_network" {
  name                = var.virtual_network_name
  resource_group_name = data.azurerm_resource_group.resource_group.name
 }

module "subnet" {
  source      = ".\\Modules\\Networking\\subnet"
  virtual_network_name  = data.azurerm_virtual_network.virtual_network.name
  resource_group_name = data.azurerm_resource_group.resource_group.name
  subnet_prefixes     = var.subnet_prefixes
  subnet_names        = var.subnet_names  
  }

#module "virtual_machine"{
#  location            = data.azurerm_resource_group.resource_group.location 
#  resource_group_name = data.azurerm_resource_group.resource_group.name
# tags = var.tags
#  vm_size = var.vm_size
#  vm_os_simple = var.vm_os_simple
#  boot_diagnostics = var.boot_diagnostics
#  boot_diagnostics_sa_type = var.boot_diagnostics_sa_type
#  vm_hostname = var.vm_hostname
#  create_option = var.create_option
#  caching = var.caching
#  storage_account_type = var.storage_account_type
#  admin_username = var.admin_username
#  admin_password = var.admin_password
#  public_ip_address_allocation = var.public_ip_address_allocation
#  vnet_subnet_id = "${module.subnet.vnet_subnets[0]}"
#  }


#module "disk_attachment" {
#  source      = ".\\Modules\\Compute\\disk_attachment"
#  managed_disk_name = var.managed_disk_name
#  location = data.azurerm_resource_group.resource_group.location 
#  resource_group_name = data.azurerm_resource_group.resource_group.name
#  storage_account_type = var.disk_storage_account_type
#  create_option = var.disk_create_option
#  disk_size_gb =  var.disk_size_gb
#  tags = var.tags
#  vm_id =  module.virtual_machine.vm_ids
#  lun_number = var.lun_number
#  disk_caching = var.disk_caching
# }
