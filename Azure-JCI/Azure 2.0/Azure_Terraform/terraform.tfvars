resource_group_name = "JCI-RG"
virtual_network_name = "AZJCI-Vnet"
tags = {
    environment = "Dev"
    costcenter = "it"
 }

subnet_prefixes = ["10.25.2.0/24"]
subnet_names = ["Backend-Subnet"]

vm_os_simple = "RHEL"
boot_diagnostics = "false"
boot_diagnostics_sa_type = "Standard_LRS"
vm_hostname = "vm-portal2"
vm_size = "Standard_B2ms"
delete_os_disk_on_termination = "false"
create_option = "FromImage"
caching = "ReadWrite"
storage_account_type = "Premium_LRS"
admin_username = "Webserveradmin"
admin_password = "C:\\Azure_TerraformScripts\\sshkeys\\publickey"

managed_disk_name = "vm-portal2-disk1"
disk_storage_account_type = "Standard_LRS"
disk_create_option = "Empty"
disk_size_gb = "10"
lun_number = "10"
disk_caching = "ReadWrite"