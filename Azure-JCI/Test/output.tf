output "vnet_id" {
  description = "The id of the newly created vNet"
  value       = "${azurerm_virtual_network.Vnet.id}"
}

output "vnet_name" {
  description = "The Name of the newly created vNet"
  value       = "${azurerm_virtual_network.Vnet.name}"
}

output "vnet_location" {
  description = "The location of the newly created vNet"
  value       = "${azurerm_virtual_network.Vnet.location}"
}

output "vnet_address_space" {
  description = "The address space of the newly created vNet"
  value       = "${azurerm_virtual_network.Vnet.address_space}"
}

output "vnet_subnets" {
  description = "The ids of subnets created inside the new vNet"
  value       = "${azurerm_subnet.subnets.*.id}"
}
