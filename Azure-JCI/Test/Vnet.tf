# Create a resource group
resource "azurerm_resource_group" "resource_group" {
  name     = var.rg_name
  location = var.location
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "Vnet" {
  name                = var.vnet_name
  resource_group_name = azurerm_resource_group.resource_group.name
  location            = azurerm_resource_group.resource_group.location
  address_space       = [var.ipv4_cidr]   
}

resource "azurerm_subnet" "subnets" {
  count                = length(var.subnets)
  name                 = element(var.subnet_name,count.index)
  resource_group_name  = azurerm_resource_group.resource_group.name
  virtual_network_name = azurerm_virtual_network.Vnet.name
  address_prefix       = element(var.subnets,count.index)
  }
  
resource "azurerm_network_security_group" "nsg_1" {
  name                = "jci-nsg-1"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name

  security_rule {
    name                       = "test123"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "nsg_2" {
  name                = "jci-nsg-2"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name

  security_rule {
    name                       = "test123"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "nsg_assoc1" {
  subnet_id                 = azurerm_subnet.subnets[0].id
  network_security_group_id = azurerm_network_security_group.nsg_1.id
}

resource "azurerm_subnet_network_security_group_association" "nsg_assoc2" {
  subnet_id                 = azurerm_subnet.subnets[1].id
  network_security_group_id = azurerm_network_security_group.nsg_1.id
}
resource "azurerm_subnet_network_security_group_association" "nsg_assoc3" {
  subnet_id                 = azurerm_subnet.subnets[2].id
  network_security_group_id = azurerm_network_security_group.nsg_2.id
}