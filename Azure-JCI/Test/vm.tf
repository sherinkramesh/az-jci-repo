resource "azurerm_network_interface" "jci_nic" {
  name                = "${var.prefix}-nic"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name

  ip_configuration {
    name                          = "${var.prefix}-${var.ip_config["name"]}"
    subnet_id                     = azurerm_subnet.subnets[0].id
    private_ip_address_allocation = var.ip_config["allocation_type"]
  }
}


resource "azurerm_virtual_machine" "jci_vm" {
  name                  = "${var.prefix}-vm"
  location              = azurerm_resource_group.resource_group.location
  resource_group_name   = azurerm_resource_group.resource_group.name
  network_interface_ids = [azurerm_network_interface.jci_nic.id]
  vm_size               = var.vm_config["size"]

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  # delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = var.vm_config["publisher"]
    offer     = var.vm_config["offer"]
    sku       = var.vm_config["sku"]
    version   = var.vm_config["version"]
  }
  storage_os_disk {
    name              = "${var.prefix}-vm-${var.vm_config["os_disk_suffix"]}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = var.vm_config["os_disk_type"]
  }
  storage_data_disk {
        name              = "${var.prefix}-vm-${var.vm_config["data_disk_suffix"]}"
        managed_disk_type = var.vm_config["data_disk_type"]
        create_option     = "Empty"
        lun               = "0"
        disk_size_gb      = "10"
    }
  os_profile {
    computer_name  = "${var.prefix}-vm"
    admin_username = var.vm_config["admin_username"]
    admin_password = var.vm_config["admin_password"]
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  tags = var.tags
}