variable "rg_name" {
  type = string
  description = "Resource Group name"
  default = "jci-rg"
 }

#################################
#Global Variable                #
#################################

variable "prefix" {
  type = string
  description = " Env name Prefix"
  default = "jci-prd"
 }

variable "location" {
  type = string
  description = "Location"
  default = "japanwest"
 }
 
#################################
#Network Variables              #
#################################

variable "vnet_name" {
  type = string
  description = "virtual Network name"
  default = "jci-vnet"
 }

variable "ipv4_cidr" {
  type = string
  description = "Virtual Network CIDR range"
  default = "10.0.0.0/16"
 }

variable "subnets" {
  type = list
  description = "Virtual Network Subnet CIDR Ranges"
  default   = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "subnet_name" {
  type = list
  description = "Virtual Network Subnet Names"
  default   = ["Web", "App", "DB"]
}

variable "tags" {
  type  = map(string)
  description = "Environment Tagging"
  default = {
    "environment" = "Production"
    "costcenter"  = "JCI-2020"
  }
}

############################################
#Virtual Machine Variables                 #
############################################

variable "ip_config" {
  type  = map(string)
  description = "IP Configuration"
  default = {
    "name"             = "ipconfig"
    "allocation_type"  = "Dynamic"
  }
}

variable "vm_config" {
  type  = map(string)
  description = "Virtual Machine Configuration"
  default = {
    "size"             = "Standard_B1ls"
    "publisher"        = "Canonical"
	"offer"			   = "UbuntuServer"
	"sku"              = "18.04-LTS"
	"version"          = "latest"
	"os_disk_suffix"   = "os_disk"
	"os_disk_type"     = "Standard_LRS"
	"admin_username"   = "jciuser"
	"admin_password"   = "Password1234!"
	"data_disk_suffix" = "data_disk"
	"data_disk_type"   = "Premium_LRS"
  }
}