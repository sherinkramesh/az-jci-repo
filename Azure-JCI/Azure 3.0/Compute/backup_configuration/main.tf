data "azurerm_virtual_machine" "vm" {
  name                = var.vmname_to_backup
  resource_group_name = var.vm_rgname_backup
 }

resource "azurerm_recovery_services_vault" "vault" {
  name                = var.recoveryservice_name
  location            = var.recoveryservice_location
  resource_group_name = var.vm_rgname_backup
  sku                 = var.recoveryservice_sku
  tags = var.recoveryservice_tags
  soft_delete_enabled = var.IsSoftDelEnabled
}

resource "azurerm_backup_policy_vm" "backuppolicy" {
    name = "${var.recoveryservice_name}-backuppolicy"
    resource_group_name = var.vm_rgname_backup
    recovery_vault_name = azurerm_recovery_services_vault.vault.name
    
    backup {
        frequency = "Daily"
        time = "23:00"
    }
    retention_daily  {
        count =14
    }
    
}
resource "azurerm_backup_protected_vm" "protectedvm" {
  resource_group_name = var.resource_group_name 
  recovery_vault_name = azurerm_recovery_services_vault.vault.name
  source_vm_id        = var.virtual_machine_id
  backup_policy_id    = azurerm_backup_policy_vm.backuppolicy.id
} 