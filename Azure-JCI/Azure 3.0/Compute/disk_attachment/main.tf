data "azurerm_virtual_machine" "vm" {
  name                = var.disk_vm_name
  resource_group_name = var.datadisk_rg_name
}

resource "azurerm_managed_disk" "vmmd" {
  name                 = "${var.managed_disk_name}"
  location             = "${var.datadisk_location}"
  resource_group_name  = "${var.datadisk_rg_name}"
  storage_account_type = "${var.disk_storage_account_type}"
  create_option        = "${var.disk_create_option}"
  disk_size_gb         = "${var.disk_size_gb}"

  tags = "${var.disk_tags}"
}


resource "azurerm_virtual_machine_data_disk_attachment" "vmmd" {
  managed_disk_id           = "${azurerm_managed_disk.vmmd.id}"
  virtual_machine_id        = data.azurerm_virtual_machine.vm.id
  lun                       = "${var.lun_number}"
  caching                   = "${var.disk_caching}"
}
