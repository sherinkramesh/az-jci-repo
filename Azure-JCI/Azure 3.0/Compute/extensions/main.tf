data "azurerm_virtual_machine" "vm" {
  name                = var.extension_vm_name
  resource_group_name = var.extension_rg_name
}


resource "azurerm_virtual_machine_extension" "vm_extension" {
  name                 = var.extension_name
  virtual_machine_id = data.azurerm_virtual_machine.vm.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  settings = jsonencode(
    {
      "fileUris": var.fileUris
    }
   )
protected_settings = jsonencode(
    {
      "commandToExecute": var.commandToExecute
      "storageAccountName": var.extension_file_location
      "storageAccountKey": var.extension_sa_key
    }
    )
 }