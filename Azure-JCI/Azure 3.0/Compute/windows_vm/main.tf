module "os" {
  source       = "./os"
  vm_os_simple = "${var.vm_os_simple}"
 }

resource "random_id" "vm-sa" {
  keepers = {
    vm_hostname = var.vm_hostname
   }

  byte_length = 6
 }

data "azurerm_subnet" "subnet" {
  name                 = var.vm_subnet_name
  virtual_network_name = var.vm_vnet_name
  resource_group_name  = var.vm_rg_name
}

resource "azurerm_storage_account" "vm-sa" {
  count                    = "${var.boot_diagnostics == "true" ? 1 : 0}"
  name                     = "bootdiag${lower(random_id.vm-sa.hex)}"
  resource_group_name      = "${var.vm_rg_name}"
  location                 = "${var.vm_location}"
  account_tier             = "${element(split("_", var.boot_diagnostics_sa_type),0)}"
  account_replication_type = "${element(split("_", var.boot_diagnostics_sa_type),1)}"
  tags                     = "${var.vm_tags}"
  }


resource "azurerm_virtual_machine" "vm-windows" {
  name                          = "${var.vm_hostname}"
  location                      = "${var.vm_location}"
  resource_group_name           = "${var.vm_rg_name}"
  vm_size                       = "${var.vm_size}"
  network_interface_ids         = ["${azurerm_network_interface.vm.id}"]
  delete_os_disk_on_termination = "${var.delete_os_disk_on_termination}"

  storage_image_reference {
    publisher = "${module.os.calculated_value_os_publisher}"
    offer     = "${module.os.calculated_value_os_offer}"
    sku       = "${module.os.calculated_value_os_sku}"
    version   = "${var.vm_os_version}"
   }

  storage_os_disk {
    name              = "osdisk-${var.vm_hostname}"
    create_option     = "${var.create_option}"
    caching           = "${var.caching}"
    managed_disk_type = "${var.storage_account_type}"
   }

  os_profile {
    computer_name  = "${var.vm_hostname}"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
   }

  tags = "${var.vm_tags}"

  os_profile_windows_config {
    provision_vm_agent = true
   }

  boot_diagnostics {
    enabled     = "${var.boot_diagnostics}"
    storage_uri = "${var.boot_diagnostics == "true" ? join(",", azurerm_storage_account.vm-sa.*.primary_blob_endpoint) : "" }"
   }
 }

data "azurerm_public_ip" "publicip" {
  name                = var.public_ip_name
  resource_group_name = var.vm_rg_name
}

resource "azurerm_network_interface" "vm" {
  name                      = "nic-${var.vm_hostname}"
  location                  = var.vm_location
  resource_group_name       = var.vm_rg_name

  ip_configuration {
    name                          = "ipconfig"
    subnet_id                     = data.azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = data.azurerm_public_ip.publicip.id
    }
}


