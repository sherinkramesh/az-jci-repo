Copy-Item .\global-variables.tf -Destination "./Account/resource_group"
Copy-Item .\terraform.tfvars -Destination "./Account/resource_group"

Copy-Item .\global-variables.tf -Destination "./Compute/windows_vm"
Copy-Item .\terraform.tfvars -Destination "./Compute/windows_vm"

Copy-Item .\global-variables.tf -Destination "./Compute/disk_attachment"
Copy-Item .\terraform.tfvars -Destination "./Compute/disk_attachment"

Copy-Item .\global-variables.tf -Destination "./Compute/extensions"
Copy-Item .\terraform.tfvars -Destination "./Compute/extensions"

Copy-Item .\global-variables.tf -Destination "./Compute/backup_configuration"
Copy-Item .\terraform.tfvars -Destination "./Compute/backup_configuration"

Copy-Item .\global-variables.tf -Destination "./Networking/network_security_group"
Copy-Item .\terraform.tfvars -Destination "./Networking/network_security_group"

Copy-Item .\global-variables.tf -Destination "./Networking/nsg_rules"
Copy-Item .\terraform.tfvars -Destination "./Networking/nsg_rules"

Copy-Item .\global-variables.tf -Destination "./Networking/public_ip"
Copy-Item .\terraform.tfvars -Destination "./Networking/public_ip"

Copy-Item .\global-variables.tf -Destination "./Networking/route_tables"
Copy-Item .\terraform.tfvars -Destination "./Networking/route_tables"

Copy-Item .\global-variables.tf -Destination "./Networking/subnet"
Copy-Item .\terraform.tfvars -Destination "./Networking/subnet"

Copy-Item .\global-variables.tf -Destination "./Networking/subnet_nsg_association"
Copy-Item .\terraform.tfvars -Destination "./Networking/subnet_nsg_association"

Copy-Item .\global-variables.tf -Destination "./Networking/subnet_routetable_association"
Copy-Item .\terraform.tfvars -Destination "./Networking/subnet_routetable_association"

Copy-Item .\global-variables.tf -Destination "./Networking/vnet"
Copy-Item .\terraform.tfvars -Destination "./Networking/vnet"