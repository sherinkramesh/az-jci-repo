variable "storage_ac_name" {}
variable "storage_location" {}
variable "conatiner" {}
variable "sa_resource_group_name" {}
variable "container_access_type"{}
variable "account_tier"{}
variable "account_replication_type" {}

provider "random" {  
  version         = "2.1.0"
 }

data  "azurerm_resource_group" "storage_rg" {
  name     = "${var.sa_resource_group_name}"
 }

resource "random_string" "storage_ac_name" {
  length = 8
  special = false
 }

resource "azurerm_storage_account" "tfstate_storage_account" {
  name                     = "tfstate${lower(random_string.storage_ac_name.result)}"
  resource_group_name      = "${data.azurerm_resource_group.storage_rg.name}"
  location                 = "${var.storage_location}"
  account_tier             = "${var.account_tier}"
  account_replication_type = "${var.account_replication_type}"
 }

resource "azurerm_storage_container" "tfstate_storage_container" {
  name                  = "${var.conatiner}"
  resource_group_name   = "${data.azurerm_resource_group.storage_rg.name}"
  storage_account_name  = "${azurerm_storage_account.tfstate_storage_account.name}"
  container_access_type = "${var.container_access_type}"
 }

output "access_key" {
  value     = "${azurerm_storage_account.tfstate_storage_account.primary_access_key}"
  sensitive = true
 }

output "storage_account_name" {
  value     = "${azurerm_storage_account.tfstate_storage_account.name}"
 }

output "sa_container_name" {
  value     = "${azurerm_storage_container.tfstate_storage_container.name}"
 }