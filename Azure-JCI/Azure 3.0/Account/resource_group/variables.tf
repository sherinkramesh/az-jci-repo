variable "resource_group_name" {
  description = "Resource group name that the resources will be created in"
 }

variable "location" {
  description = "The location/region where the resources will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
 }

variable "tags" {
  type    = map
  default = {}
 }
