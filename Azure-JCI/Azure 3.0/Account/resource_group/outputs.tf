output "resource_group_id" {
  description = "The id of the newly created resource group"
  value       = "${azurerm_resource_group.rg.id}"
}

output "resource_group_name" {
  description = "The name of the newly created resource group"
  value       = "${azurerm_resource_group.rg.name}"
}
