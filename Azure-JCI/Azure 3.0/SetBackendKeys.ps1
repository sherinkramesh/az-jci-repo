# Apply terraform to create new storage account, this outputs storage account name and access key
# these will be used to set environment variables for use in demo script

#Set-ExecutionPolicy -ExecutionPolicy Restricted -Scope CurrentUser
#powershell -ExecutionPolicy Unrestricted

terraform apply -auto-approve

$storageAccountName = terraform.exe output storage_account_name
$container = terraform.exe output sa_container_name
$accesskey = terraform.exe output access_key

if ($storageAccountName -and $accesskey) {
  [Environment]::SetEnvironmentVariable("TF_VAR_storage_ac", $storageAccountName, "User")
  [Environment]::SetEnvironmentVariable("TF_VAR_container", $container, "User")  
  [Environment]::SetEnvironmentVariable("TF_VAR_access_key", $accesskey, "User")  
}
else {
  Write-Error "Unable to get storage_account_name,container and access_key from terraform output, did terraform apply okay?"
}
