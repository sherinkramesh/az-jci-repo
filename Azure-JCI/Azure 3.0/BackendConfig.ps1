param ([string]$tfstate_name)

$Folder=".\.terraform\"

If (Test-Path $Folder)
{
 Remove-Item $Folder -Recurse
}

$storageAC = [Environment]::GetEnvironmentVariable("TF_VAR_storage_ac",'User')
$accessKey = [Environment]::GetEnvironmentVariable("TF_VAR_access_key",'User')
$container = [Environment]::GetEnvironmentVariable("TF_VAR_container",'User')
terraform init -backend-config="storage_account_name=$StorageAC"  -backend-config="container_name=$container"  -backend-config="key=$tfstate_name" -backend-config="access_key=$accessKey"