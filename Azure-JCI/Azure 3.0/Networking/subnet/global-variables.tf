variable "subnet_prefixes" {
 default = []
 }
variable "subnet_names" {
 default = []
 }
variable "subnet_vnet_name" {
 default = ""
 }
variable "subnet_rg_name" {
 default = ""
 }