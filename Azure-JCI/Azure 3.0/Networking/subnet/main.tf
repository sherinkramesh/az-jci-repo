data "azurerm_resource_group" "get_resource_group" {
  name = var.subnet_rg_name
}

data "azurerm_virtual_network" "get_virtual_network" {
  name                = var.subnet_vnet_name
  resource_group_name = var.subnet_rg_name
}

resource "azurerm_subnet" "subnet" {
  name                      = var.subnet_names[count.index]
  virtual_network_name      = data.azurerm_virtual_network.get_virtual_network.name
  resource_group_name       = data.azurerm_resource_group.get_resource_group.name
  address_prefix            = var.subnet_prefixes[count.index]
  count                     = length(var.subnet_names)
}
