data "azurerm_subnet" "subnet" {
  name                 = var.subnet_name_to_route
  virtual_network_name = var.subnet_route_vnet_name
  resource_group_name  = var.subnet_nsg_rg_name
}

data "azurerm_route_table" "routetable" {
  name                = var.routetable_to_subnet_name
  resource_group_name = var.subnet_route_rg_name
}

resource "azurerm_subnet_route_table_association" "subnetroute" {
  subnet_id      = data.azurerm_subnet.subnet.id
  route_table_id = data.azurerm_route_table.routetable.id
}