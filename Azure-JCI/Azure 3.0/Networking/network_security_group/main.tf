data "azurerm_resource_group" "nsg_resource_group" {
  name = var.subnet_rg_name
}

resource "azurerm_network_security_group" "nsg" {
  name                = var.nsg_name
  location            = var.nsg_location
  resource_group_name = data.azurerm_resource_group.nsg_resource_group.name
  tags                = var.nsg_tags
}