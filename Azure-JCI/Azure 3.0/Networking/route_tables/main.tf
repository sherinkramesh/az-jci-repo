data "azurerm_resource_group" "route_resource_group" {
  name = var.route_rg_name
}

data "azurerm_virtual_network" "get_virtual_network" {
  name                = var.subnet_vnet_name
  resource_group_name = var.subnet_rg_name
}

resource "azurerm_route_table" "route_table" {
  name                          = var.routetable_name
  location                      = var.route_location
  resource_group_name           = data.azurerm_resource_group.route_resource_group.name
  disable_bgp_route_propagation = var.bgp_route_propagation

  route {
    name           = var.route_name
    address_prefix = var.route_address_prefix
    next_hop_type  = var.next_hop_type
    #next_hop_in_ip_address = var.next_hop_in_ip_address
  }

  tags = var.route_tags
}