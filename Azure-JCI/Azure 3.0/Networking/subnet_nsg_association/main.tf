data "azurerm_subnet" "subnet" {
  name                 = var.subnet_name_to_nsg
  virtual_network_name = var.subnet_nsg_vnet_name
  resource_group_name  = var.subnet_nsg_rg_name
}

data "azurerm_network_security_group" "nsg" {
  name                = var.nsg_name_to_subnet
  resource_group_name = var.subnet_nsg_rg_name
}

resource "azurerm_subnet_network_security_group_association" "sunbet_nsg_associate" {
  subnet_id                 = data.azurerm_subnet.subnet.id
  network_security_group_id = data.azurerm_network_security_group.nsg.id
}