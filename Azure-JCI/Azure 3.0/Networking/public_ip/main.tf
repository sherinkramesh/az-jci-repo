data "azurerm_resource_group" "publicip_rg" {
  name = var.publicIp_rg_name
}

resource "azurerm_public_ip" "publicIp" {
  name                         = var.publicIp_name
  location                     = var.publicIp_location
  resource_group_name          = data.azurerm_resource_group.publicip_rg.name
  allocation_method            = var.publicIp_allocation_method
 }