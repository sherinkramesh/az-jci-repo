data "azurerm_resource_group" "nsg_rule_rg" {
  name = var.nsgrule_rg_name
}

data "azurerm_network_security_group" "secutiyrules_nsg" {
  name                = var.securityrule_nsg_name
  resource_group_name = var.nsgrule_rg_name
}


resource "azurerm_network_security_rule" "rule" {
  resource_group_name                        = data.azurerm_resource_group.nsg_rule_rg.name
  network_security_group_name                = data.azurerm_network_security_group.secutiyrules_nsg.name
  name                                       = var.securityrule_name
  priority                                   = var.rule_priority
  direction                                  = var.rule_direction
  access                                     = var.rule_access
  protocol                                   = var.rule_protocol
  source_port_range                          = var.source_port_range
  destination_port_ranges                   = var.destination_port_ranges
  source_address_prefix                      = var.source_address_prefix
  source_application_security_group_ids      = var.source_nsg_ids
  destination_address_prefix                 = var.destination_address_prefix
  destination_application_security_group_ids = var.destination_nsg_ids
  description                                = var.rule_description
}