data "azurerm_resource_group" "get_resource_group" {
  name = var.vnet_rg_name
}

resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  location            = var.vnet_location
  address_space       = var.address_space
  resource_group_name = data.azurerm_resource_group.get_resource_group.name
  dns_servers         = var.dns_servers
  tags                = var.vnet_tags
}