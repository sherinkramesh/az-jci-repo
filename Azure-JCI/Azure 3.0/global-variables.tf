variable client_id {}
variable client_secret {}
variable tenant_id {}
variable subscription_id {}

#############################################################################
# Resource Group Variables
#############################################################################
variable "resource_group_name" {
 default = ""
 }
variable "location" {
 default = ""
 }
variable "tags" {
 default = {}
 }
#############################################################################
# Virtual Network Variables
#############################################################################
variable "vnet_name" {
 default = ""
 } 
variable "vnet_rg_name" {
 default = ""
 }
variable "vnet_location" {
 default = ""
 }
variable "address_space" {
 default = []
 }
variable "dns_servers" {
 default = []
 }
variable "vnet_tags" {
 default = {}
 }
#############################################################################
# Subnet Variables
#############################################################################
variable "subnet_prefixes" {
 default = []
 }
variable "subnet_names" {
 default = []
 }
variable "subnet_vnet_name" {
 default = ""
 }
variable "subnet_rg_name" {
 default = ""
 }

#############################################################################
# Network Security group Variables
#############################################################################
variable "nsg_name" {
 default = ""
 } 
variable "nsg_rg_name" {
 default = ""
 }
variable "nsg_location" {
 default = ""
 }

variable "nsg_tags" {
 default = {}
 }
 
#############################################################################
# NSG Security Rules Variables
#############################################################################
variable "securityrule_name" {
 default = ""
 } 

variable "securityrule_nsg_name" {
 default = ""
 } 
variable "nsgrule_rg_name" {
 default = ""
 }
variable "rule_priority" {
 }

variable "rule_direction" {
 default = "Inbound"
 }

variable "rule_access" {
 default = "Allow"
 }

variable "rule_protocol" {
  default = "Tcp"
 }

variable "source_port_range" {
  default = "*"
 }

variable "destination_port_ranges" {
  default = ["*"]
 }

variable "source_address_prefix" {
  default = "*"
 }

variable "source_nsg_ids" {
  default = []
 }

variable "destination_address_prefix" {
  default = "*"
 }

variable "destination_nsg_ids" {
  default = []
 }

variable "rule_description" {
  default = ""
 }
 
#############################################################################
# Subnet and NSG Association Variables
#############################################################################
variable "subnet_name_to_nsg" {
 default = ""
 } 
 
variable "nsg_name_to_subnet" {
 default = ""
 } 
 
variable "subnet_nsg_vnet_name" {
 default = ""
 } 
 
variable "subnet_nsg_rg_name" {
 default = ""
 }
 


#############################################################################
# Route Table Variables
#############################################################################
variable "routetable_name" {
 default = ""
 } 
variable "route_rg_name" {
 default = ""
 }
variable "route_location" {
 default = ""
 }

variable "route_tags" {
 default = {}
 }
 
variable "route_name" {
  default = ""
 }

variable "route_address_prefix" {
  default = ""
 }

variable "next_hop_type" {
 default = "None" 
 }
variable "bgp_route_propagation" {
  default = true
 }

variable "next_hop_in_ip_address" {
  default = ""
 }
 
#############################################################################
# Subnet and RouteTable Association Variables
#############################################################################
variable "subnet_name_to_route" {
 default = ""
 } 
 
variable "routetable_to_subnet_name" {
 default = ""
 } 
 
variable "subnet_route_vnet_name" {
 default = ""
 } 
 
variable "subnet_route_rg_name" {
 default = ""
 }
 
#############################################################################
# Public IP Variables
#############################################################################
variable "publicIp_rg_name" {
 default = ""
 }
variable "publicIp_location" {
 default = ""
 }
variable "publicIp_name" {
 default = ""
 }
variable "publicIp_allocation_method" {
 default = "Dynamic"
 }


#############################################################################
# Virtual Machines Variables
#############################################################################

variable "vm_rg_name" {
 default = ""
 }
variable "vm_location" {
 default = ""
 }

variable "vm_os_simple" {
 default = ""
 }

variable "vm_hostname" {
 default = ""
 }

variable "boot_diagnostics" {
 default = "false"
 }

variable "boot_diagnostics_sa_type" {
 default = "Standard_LRS"
 }

variable "vm_tags" {
 default = {}
 }

variable "vm_os_version" {
 default = "latest"
 }
variable "vm_size" {
 default = "Standard_B1S"
 }
variable "delete_os_disk_on_termination" {
 default = true
 } 

variable "create_option" {
 default = "FromImage"
 }

variable "caching" {
 default = "ReadWrite"
 }
variable "storage_account_type" {
 default = "Premium_LRS"
 }
variable "admin_username" {
 default = ""
 }

variable "admin_password" {
 default = ""
 }
 
variable "vm_subnet_name" {
 default = []
 }
variable "vm_vnet_name" {
 default = ""
 }
variable "public_ip_name" {
 default = ""
 }

#############################################################################
# disk_attachment Variables
#############################################################################


variable "managed_disk_name" {
  default = ""
 }

variable "datadisk_location" {
  default = ""
 }

variable "datadisk_rg_name" {
  default = ""
 }

variable "disk_vm_name" {
  default = ""
 }

variable "disk_storage_account_type" {
   default = "Standard_LRS"
 }

variable "disk_create_option" {
  default     = "Empty"
 }

variable "disk_size_gb" {  
  default     = "10"
 }



variable "lun_number" {
  default     = "0"
 }

variable "disk_caching" {
  default     = "ReadWrite"
 }
 

variable "disk_tags" {
 default = {}
 }
 
 #############################################################################
# VM Extension Variables
##############################################################################

variable "extension_name" {}
variable "extension_vm_name" {
  default = ""
 }
variable "extension_rg_name" {
  default = ""
 }
 
variable "fileUris" {
  type = list
 }

variable "commandToExecute" {}
variable "extension_file_location" {}
variable "extension_sa_key" {}