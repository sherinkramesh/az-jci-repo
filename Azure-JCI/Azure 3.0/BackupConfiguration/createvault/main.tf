resource "azurerm_recovery_services_vault" "vault" {
  name                = var.recoveryservice_name
  location            = var.recoveryservice_location
  resource_group_name = var.recoveryservice_rg_name
  sku                 = var.recoveryservice_sku
  tags = var.recoveryservice_tags
  soft_delete_enabled = var.IsSoftDelEnabled
}