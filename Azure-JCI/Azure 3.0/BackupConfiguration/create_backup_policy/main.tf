data "azurerm_recovery_services_vault" "vault" {
  name                = var.backuppolicy_vault_name
  resource_group_name = var.backuppolicy_rg_name
}

resource "azurerm_backup_policy_vm" "backuppolicy" {
    name = var.backup_policy_name
    resource_group_name = var.resource_group_name
    recovery_vault_name = azurerm_recovery_services_vault.vault.name
    
    backup {
        frequency = var.frequency
        time = var.backup_time
    }
    retention_daily  {
        count =var.retention_count
    }
    
}