data "azurerm_recovery_services_vault" "vault" {
  name                = var.vm_backupvault_name
  resource_group_name = var.vm_backup_rg_name
}

data "azurerm_backup_policy_vm" "policy" {
  name                = var.vm_backuppolicy_name
  recovery_vault_name = var.vm_backup_rg_name
  resource_group_name = "resource_group"
}

resource "azurerm_backup_protected_vm" "protectedvm" {
  resource_group_name = var.resource_group_name 
  recovery_vault_name = azurerm_recovery_services_vault.vault.name
  source_vm_id        = var.virtual_machine_id
  backup_policy_id    = azurerm_backup_policy_vm.policy.id
} 