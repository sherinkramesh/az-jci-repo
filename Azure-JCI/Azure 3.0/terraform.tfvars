storage_ac_name = "tfstates-sa-002"
conatiner       = "tfstate-files-002"
storage_location = "eastus2"
sa_resource_group_name = "rg-storage-shared-002"
container_access_type = "private"
account_tier = "Standard"
account_replication_type = "LRS"

#virtual network  values
vnet_name = "JCI-Vnet"
address_space = ["9.0.0.0/16" ]
vnet_rg_name = "rg-storage-shared-001"
vnet_location = "eastus2"
vnet_tags = {
    environment = "Dev"
    costcenter = "it"   
 }

#subnet values
subnet_prefixes = ["9.0.2.0/24"]
subnet_names = ["Backend"]
subnet_vnet_name = "JCI-Vnet"
subnet_rg_name = "rg-storage-shared-001"

#publicIp values
publicIp_rg_name = "rg-storage-shared-001"
publicIp_location = "eastus2"
publicIp_name = "vm1-publicip"
publicIp_allocation_method = "Dynamic"

#NSG values
nsg_name = "jci-nsg"
nsg_rg_name = "rg-storage-shared-001"
nsg_location = "eastus2"
nsg_tags ={
    environment = "Dev"
    costcenter = "it"   
 }
 
#NSG Rules values
securityrule_name = "httpPorts"
securityrule_nsg_name = "jci-nsg"
nsgrule_rg_name = "rg-storage-shared-001"
rule_priority = 100
rule_direction = "Inbound"
destination_port_ranges = ["80","443","3389"]
rule_description = "This Rule is created to enable the required port number"
 

#subnet to nsg values
subnet_name_to_nsg = "Frontend"
nsg_name_to_subnet = "jci-nsg"
subnet_nsg_vnet_name =  "JCI-Vnet"
subnet_nsg_rg_name = "rg-storage-shared-001"

#Route Table values
routetable_name = "BackendUDR"
route_rg_name = "rg-storage-shared-001"
route_location = "eastus2"
route_tags = {
    environment = "Dev"
    costcenter = "it"   
 }
route_name = "Backendroutes"
next_hop_type = "VnetLocal"
route_address_prefix = "9.0.1.0/24"

#subnet to ROuteTable values
subnet_name_to_route = "Backend"
routetable_to_subnet_name = "BackendUDR"
subnet_route_vnet_name = "JCI-Vnet"
subnet_route_rg_name = "rg-storage-shared-001"


#vm values
vm_rg_name = "rg-storage-shared-001"
vm_location = "eastus2"
vm_os_simple ="WindowsServer"
vm_size = "Standard_B2ms"
vm_hostname = "vm-portal1"
vm_tags = {
    environment = "Dev"
    costcenter = "it"   
 }
admin_username = "Webserveradmin"
admin_password = "Password123!"
vm_subnet_name ="Frontend"
vm_vnet_name = "JCI-Vnet"
public_ip_name = "vm1-publicip"

#Data dsk values
managed_disk_name = "vm-portal1-Disk1"
datadisk_location= "eastus2"
datadisk_rg_name = "rg-storage-shared-001"
disk_vm_name ="vm-portal1"
disk_storage_account_type = "Standard_LRS"
disk_create_option = "Empty"
disk_size_gb = "10"
lun_number = "10"
disk_caching = "ReadWrite"
disk_tags = {
    environment = "Dev"
    costcenter = "it"   
 }
 
#extension values
extension_name = "EnableIIS"
fileUris = ["https://tfstater73lqchu.blob.core.windows.net/extensions/EnableIIS.ps1"]
commandToExecute = "powershell -ExecutionPolicy Unrestricted -File EnableIIS.ps1"
extension_file_location = "tfstater73lqchu"
extension_sa_key = "rNwN+X0WJ0Av9QV6o9OpH74oCso0EW3GH/koOrLgQenaPAwOFrr+nrOvIAkw2v+buctbcSdioEjtolvtfeZotg==" 
extension_vm_name = "vm-portal1"
extension_rg_name = "rg-storage-shared-001"
