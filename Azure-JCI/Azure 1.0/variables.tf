#############################################################################
# Resource Group Variables
#############################################################################

variable "resource_group_name" {    
  description = "Resource group name that the resources will be created in"
 }
variable "location" {
    description = "The location/region where the resources will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
 }
variable "tags" {
    description = "Tags details that the resources will be assigned with"
 }

#############################################################################
# Network Security Group Variables
#############################################################################
variable "nsg_name" {
  description = "Specifies the name of the network security group"
  type = string
 }

 #############################################################################
# Network Security Group Rules Variables
#############################################################################
variable "rule_name" {
  description = "The name of the security rule. This needs to be unique across all Rules in the Network Security Group"
 }

variable "rule_priority" {
  description = "Specifies the priority of the rule. The value can be between 100 and 4096. The priority number must be unique for each rule in the collection"
 }

variable "rule_direction" {
  description = "The direction specifies if rule will be evaluated on incoming or outgoing traffic. Possible values are Inbound and Outbound"
 }

variable "rule_access" {
  description = "Specifies whether network traffic is allowed or denied. Possible values are Allow and Deny"
  default     = "Allow"
 }

variable "rule_protocol" {
  description = "Network protocol this rule applies to. Possible values include Tcp, Udp or * "
  default     = "Tcp"
 }

variable "source_port_range" {
  description = "Source Port or Range. Integer or range between 0 and 65535 or * to match any.This is required if source_port_ranges is not specified"
  default     = "*"
 }

variable "destination_port_range" {
  description = "Destination Port or Range. Integer or range between 0 and 65535 or * to match any. This is required if destination_port_ranges is not specified"
  default     = "*"
 }

variable "source_address_prefix" {
  description = "CIDR or source IP range or * to match any IP. Tags such as ‘VirtualNetwork’, ‘AzureLoadBalancer’ and ‘Internet’ can also be used. This is required if source_address_prefixes is not specified"
  default     = "*"
 }

variable "source_nsg_ids" {
  description = "A List of source Application Security Group ID's"
  default     = []
 }

variable "destination_address_prefix" {
  description = "CIDR or destination IP range or * to match any IP. Tags such as ‘VirtualNetwork’, ‘AzureLoadBalancer’ and ‘Internet’ can also be used. This is required if destination_address_prefixes is not specified"
  default     = "*"
 }

variable "destination_nsg_ids" {
  description = "A List of destination Application Security Group ID's"
  default     = []
 }

variable "rule_description" {
  description = "A description for this rule. Restricted to 140 characters"
  default     = ""
 }

#############################################################################
# Virtual Network															#
#############################################################################
variable "vnet_name" {
  description = "Name of the vnet to create"
 }

variable "address_space" {
  description = "The address space that is used by the virtual network"
 }

# If no values specified, this defaults to Azure DNS
variable "dns_servers" {
  description = "The DNS servers to be used with vNet"
  default     = []
 }

#############################################################################
# Subnet Variables
#############################################################################

variable "subnet_prefixes" {
  description = "The address prefix to use for the subnet"
  default     = []
 }

variable "subnet_names" {
  description = "A list of public subnets inside the vNet"
  default     = []
 }

variable "nsg_ids" {
  description = "A map of subnet name to Network Security Group IDs"
   type        = map
   default = {}
  }

variable "service_endpoints" {
  description = "The list of Service endpoints to associate with the subnet. Possible values include: Microsoft.Storage, Microsoft.Sql"
  default     = []
  }

#############################################################################
# Route Table Variables
##############################################################################
variable "route_table_name" {
 description = "Name of the Route Table"
 default = ""
 }

variable "route_name" {
  description = "Name of the route"
  default = ""
 }

variable "route_address_prefix" {
  description = "The Address space of the route to redirect the trafic"
  default = ""
 }

variable "next_hop_type" {
  description = "The type of Azure hop the packet should be sent to. Possible values are VirtualNetworkGateway, VnetLocal, Internet, VirtualAppliance and None."
  default = "None" 
 }
variable "bgp_route_propagation" {
  description = "The tags to associate with your route table"
  default = true
 }

variable "next_hop_in_ip_address" {
 default = ""
 }
  
#############################################################################
# Virtual Machines Variables
#############################################################################

variable "vm_os_simple" {
  description = "Specify UbuntuServer, WindowsServer, RHEL, openSUSE-Leap, CentOS, Debian, CoreOS and SLES to get the latest image version of the specified os.  Do not provide this value if a custom value is used for vm_os_publisher, vm_os_offer, and vm_os_sku."
  default     = ""
 }

variable "vm_hostname" {
  description = "local name of the VM"
 }

variable "boot_diagnostics" {
  description = "(Optional) Enable or Disable boot diagnostics"
  default     = "false"
 }

variable "boot_diagnostics_sa_type" {
  description = "(Optional) Storage account type for boot diagnostics"
  default     = "Standard_LRS"
 }


variable "vm_os_id" {
  description = "The resource ID of the image that you want to deploy if you are using a custom image.Note, need to provide is_Custom_image = true for windows custom images"
  default     = ""
 }
 variable "is_Custom_image" {
  description = "Boolean flag to notify when the custom image is windows based. Only used in conjunction with vm_os_id"
  default     = "false"
 }

variable "vm_os_offer" {
  description = "The name of the offer of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = ""
 }
variable "data_disk" {
  type        = string
  description = "Set to true to add a datadisk."
  default     = "false"
 }
variable "vm_size" {
  description = "Specifies the size of the virtual machine"
  default     = "Standard_B1S"
 }
variable "delete_os_disk_on_termination" {
  description = "Delete datadisk when machine is terminated"
  default     = "false"
 }
variable "vm_os_version" {
  description = "The version of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = "latest"
 }
variable "create_option" {
  description = "Specifies how the OS Disk should be created. Possible values are Attach (managed disks only) and FromImage"
  default     = "FromImage"
 }

variable "caching" {
  description = "Specifies the caching requirements for the OS Disk. Possible values include None, ReadOnly and ReadWrite"
  default     = "ReadWrite"
 }
variable "storage_account_type" {
  description = "Defines the type of storage account to be created. Valid options are Standard_LRS, Standard_ZRS, Standard_GRS, Standard_RAGRS, Premium_LRS"
  default     = "Premium_LRS"
 }
variable "admin_username" {
  description = "The admin username of the VM that will be deployed"
  default     = "azureuser"
 }

variable "admin_password" {
  description = "The admin password to be used on the VMSS that will be deployed. The password must meet the complexity requirements of Azure"
  default     = ""
 }

variable "fault_domain_count" {
  description = "Specifies the number of fault domains that are used. Max 3 for now"
  default     = "2"
 }

variable "update_domain_count" {
  description = "Specifies the number of update domains that are used. Max 20 for now"
  default     = "2"
 }
variable "public_ip_address_allocation" {
  description = "Defines how an IP address is assigned. Options are Static or Dynamic."
  default     = "Dynamic"
 }
variable "nsg_id" {
  description = "NSG IF to be attached with NIC"
  default     = ""
 }
variable "vnet_subnet_id" {
  description = "The subnet id of the virtual network where the virtual machines will reside"
  default = ""
 }

variable "vm_os_publisher" {
  default     = ""
 }
variable "vm_os_sku" {
  default     = ""
 }

#############################################################################
# disk_attachment Variables
#############################################################################

variable "managed_disk_name" {
  default = ""
 }

variable "disk_storage_account_type" {
   default = "Standard_LRS"
 }

variable "disk_create_option" {
  default     = "Empty"
 }

variable "disk_size_gb" {  
  default     = "10"
 }

variable "is_encryption_enabled" {
  default     = true
 }

variable "secret_url" {
  default = ""
 }

variable "source_vault_id" {
  default = ""
 }

variable "lun_number" {
  default     = "0"
 }


 variable "disk_caching" {
  description = " Specifies the caching requirements for this Data Disk. Possible values include None, ReadOnly and ReadWrite"
  default     = "ReadWrite"
 }

variable "write_accelerator_enabled" {
  default     = false
 }

#############################################################################
# Backup configuration Variables
#############################################################################

variable IsSoftDelEnabled {
  default = true
 }

variable "recovery_service_name" {
  type    = string
 }

variable "recovery_service_sku" {
  type    = string
  default = "Standard"
 }

##############################################################################
# metric alert rule Variables
##############################################################################

variable "action_group_name" {
  default = ""
 }

variable "custom_emails" {
  type        = string
  default     = ""
 }
variable "metric_rule_name" {
  description = "Specifies the name of the alert rule"
  default= ""
 }

variable "alert_description" {
  description = "A verbose description of the alert rule that will be included in the alert email"
 }
variable "metric_name" {
  description = "The metric that defines what the rule monitors. For list of mrtic names, refer https://docs.microsoft.com/en-us/azure/monitoring-and-diagnostics/monitoring-supported-metrics"
 }

variable "metric_operator" {
  description = "The operator used to compare the metric data and the threshold. Possible values are GreaterThan, GreaterThanOrEqual, LessThan, and LessThanOrEqual"
 }

variable "threshold" {
  description = "The threshold value that activates the alert"
 }

variable "aggregation" {
  description = "Defines how the metric data is combined over time. Possible values are Average, Minimum, Maximum, Total, and Last"
  default     = "Average"
 }

