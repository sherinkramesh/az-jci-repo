module "resource_group" {
    source      = ".\\Modules\\Account\\Resource_Group"
    resource_group_name = var.resource_group_name
    location = var.location
    tags     = var.tags
 }

module "vnet" {
  source      = ".\\Modules\\Networking\\vnet"
  vnet_name           = var.vnet_name
  location            = var.location  
  resource_group_name = module.resource_group.resource_group_name
  tags                = var.tags
  address_space       = var.address_space
  }

module "subnet" {
  source      = ".\\Modules\\Networking\\subnet"
  virtual_network_name  = module.vnet.vnet_name
  resource_group_name = module.resource_group.resource_group_name
  subnet_prefixes     = var.subnet_prefixes
  subnet_names        = var.subnet_names  
  }

module "nsg" {
    source      = ".\\Modules\\Networking\\network_security_group"
    resource_group_name = module.resource_group.resource_group_name
    location = var.location
    tags     = var.tags
    nsg_name = var.nsg_name
 }

module "nsg_security_rules" {
  source      = ".\\Modules\\Networking\\nsg_rules"
  resource_group_name = module.resource_group.resource_group_name
  nsg_name            = module.nsg.nsg_name
  rule_name           = var.rule_name
  rule_priority       = var.rule_priority
  rule_direction      = var.rule_direction
  destination_port_range   = var.destination_port_range
  rule_description    = var.rule_description
 }

module "sunbet_nsg_associate" {
  source      = ".\\Modules\\Networking\\subnet_nsg_association"
  subnet_id = module.subnet.vnet_subnets[0]
  network_security_group_id = module.nsg.nsg_id
 }
module "route_table" {
  source      = ".\\Modules\\Networking\\route_tables"
  route_table_name = var.route_table_name
  location = var.location
  resource_group_name = module.resource_group.resource_group_name
  route_name = var.route_name
  route_address_prefix = "${var.subnet_prefixes[0]}"
  next_hop_type = var.next_hop_type
  tags = var.tags
  #next_hop_in_ip_address = var.next_hop_in_ip_address
 }

module "sunbet_routetable_associate" {
  source      = ".\\Modules\\Networking\\subnet_routetable_association"
  subnet_id = module.subnet.vnet_subnets[0]
  route_table_id = module.route_table.route_table_id
 }

module "virtual_machine"{
  source      = ".\\Modules\\Compute\\windows_vm"
  #source      = ".\\Modules\\Compute\\linux_vm"
  location            = var.location  
  resource_group_name = module.resource_group.resource_group_name
  tags = var.tags
  vm_size = var.vm_size
  vm_os_simple = var.vm_os_simple
  boot_diagnostics = var.boot_diagnostics
  boot_diagnostics_sa_type = var.boot_diagnostics_sa_type
  vm_hostname = var.vm_hostname
  create_option = var.create_option
  caching = var.caching
  storage_account_type = var.storage_account_type
  admin_username = var.admin_username
  admin_password = var.admin_password
  #fault_domain_count = var.fault_domain_count
  #update_domain_count = var.update_domain_count
  public_ip_address_allocation = var.public_ip_address_allocation
  nsg_id = module.nsg.nsg_id 
  vnet_subnet_id = "${module.subnet.vnet_subnets[0]}"
  }


module "disk_attachment" {
  source      = ".\\Modules\\Compute\\disk_attachment"
  managed_disk_name = var.managed_disk_name
  location = var.location
  resource_group_name = module.resource_group.resource_group_name
  storage_account_type = var.disk_storage_account_type
  create_option = var.disk_create_option
  disk_size_gb =  var.disk_size_gb
   #is_encryption_enabled = 
    #secret_url = 
    #source_vault_id = 
  tags = var.tags
  vm_id =  module.virtual_machine.vm_ids
  lun_number = var.lun_number
  disk_caching = var.disk_caching
 }

module "metric_alert_rule"{
  source      = ".\\Modules\\Compute\\metric_alert_rule"
  resource_group_name = module.resource_group.resource_group_name
  tags = var.tags
  action_group_name = var.action_group_name
  metric_rule_name = var.metric_rule_name
  scope =module.virtual_machine.vm_ids
  alert_description = var.alert_description
  metric_name = var.metric_name
  metric_operator = var.metric_operator
  threshold = var.threshold
  aggregation = var.aggregation  
  custom_emails = var.custom_emails
 }

#module "Configure_Backup_recovery" {
#   source      = ".\\Modules\\Recovery"
#  location = var.location
#  tags = var.tags
#  resource_group_name = module.resource_group.resource_group_name
#  recovery_service_name = var.recovery_service_name
#  recovery_service_sku = var.recovery_service_sku
# virtual_machine_id = module.virtual_machine.vm_ids
# }
