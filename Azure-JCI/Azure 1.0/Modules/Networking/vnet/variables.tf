variable "vnet_name" {
  description = "Name of the vnet to create"
 }

variable "resource_group_name" {
  description = "Resource group name that the network will be created in"
 }

variable "location" {
  description = "The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
 }

variable "address_space" {
  description = "The address space that is used by the virtual network"
 }

# If no values specified, this defaults to Azure DNS
variable "dns_servers" {
  description = "The DNS servers to be used with vNet"
  type = list(string)
  default     = []
 }

variable "tags" {
  description = "The tags to associate with your network and subnets"
  type        = map
  default = {}
 }

