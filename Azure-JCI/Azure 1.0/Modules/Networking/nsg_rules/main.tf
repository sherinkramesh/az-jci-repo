resource "azurerm_network_security_rule" "rule" {
  resource_group_name                        = var.resource_group_name
  network_security_group_name                = var.nsg_name
  name                                       = var.rule_name
  priority                                   = var.rule_priority
  direction                                  = var.rule_direction
  access                                     = var.rule_access
  protocol                                   = var.rule_protocol
  source_port_range                          = var.source_port_range
  destination_port_range                     = var.destination_port_range
  source_address_prefix                      = var.source_address_prefix
  source_application_security_group_ids      = var.source_nsg_ids
  destination_address_prefix                 = var.destination_address_prefix
  destination_application_security_group_ids = var.destination_nsg_ids
  description                                = var.rule_description
}