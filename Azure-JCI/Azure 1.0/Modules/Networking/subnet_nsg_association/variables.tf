variable "subnet_id" {
  description = "Name of the subnet id  to associate with nsg"
  default = ""
}

variable "network_security_group_id" {
  description = "Name of the network_security_group_id   to associate with subnet"
  default = ""
}
