resource "azurerm_route_table" "route_table" {
  name                          = var.route_table_name
  location                      = var.location
  resource_group_name           = var.resource_group_name
  disable_bgp_route_propagation = var.bgp_route_propagation

  route {
    name           = var.route_name
    address_prefix = var.route_address_prefix
    next_hop_type  = var.next_hop_type
    #next_hop_in_ip_address = var.next_hop_in_ip_address
  }

  tags = var.tags
}