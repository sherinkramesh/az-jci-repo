resource "azurerm_recovery_services_vault" "vault" {
  name                = var.recovery_service_name
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = var.recovery_service_sku
  tags = var.tags
  soft_delete_enabled = var.IsSoftDelEnabled
}

resource "azurerm_backup_policy_vm" "backuppolicy" {
    name = "${var.recovery_service_name}-backuppolicy"
    resource_group_name = var.resource_group_name
    recovery_vault_name = azurerm_recovery_services_vault.vault.name
    
    backup {
        frequency = "Daily"
        time = "23:00"
    }
    retention_daily  {
        count =14
    }
    
}
resource "azurerm_backup_protected_vm" "protectedvm" {
  resource_group_name = var.resource_group_name 
  recovery_vault_name = azurerm_recovery_services_vault.vault.name
  source_vm_id        = var.virtual_machine_id
  backup_policy_id    = azurerm_backup_policy_vm.backuppolicy.id
} 