variable "resource_group_name" {
  description = "Resource group name that the resources will be created in"
  type    = string
 }

variable "location" {
  description = "The location/region where the resources will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
  type    = string
 }

variable "tags" {
  type    = map
  default = {}
 }


variable IsSoftDelEnabled {
  description = "Specify the value whether to enable teh soft delete or not"
  default = true
 }

variable "recovery_service_name" {
  description = "Name of the azurerm recovery services vault "
  type    = string
 }

variable "recovery_service_sku" {
  description = "SKU name of the azurerm recovery services vault "
  type    = string
  default = "Standard"
 }
variable "virtual_machine_id" {
  description = "Id of the Vm that are going to associated with Backup recovery "
  type    = string
 }