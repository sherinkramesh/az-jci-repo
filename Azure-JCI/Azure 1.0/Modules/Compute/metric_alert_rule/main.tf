resource "azurerm_monitor_action_group" "action_group" {
  name                = var.action_group_name
  resource_group_name = var.resource_group_name
  short_name          = var.action_group_name

  email_receiver {
    name          = "emailsender"
    email_address = var.custom_emails
  }
}


resource "azurerm_monitor_metric_alert" "example" {
  name                = var.metric_rule_name
  resource_group_name = var.resource_group_name
  scopes              = [var.scope]
  description         = var.alert_description

  criteria {
    metric_namespace = "microsoft.compute/virtualmachines"
    metric_name      = var.metric_name
    aggregation      = var.aggregation
    operator         = var.metric_operator
    threshold        = var.threshold

  }

  action {
    action_group_id = azurerm_monitor_action_group.action_group.id
  }
}