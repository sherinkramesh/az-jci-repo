variable "managed_disk_name" {
  description = "Specifies the name of the managed disk"
 }

 variable "storage_account_type" {
  description = "The type of storage to use for the managed disk. Allowable values are Standard_LRS or Premium_LRS"
 }

variable "create_option" {
  description = "The method to use when creating the managed disk. Possible values include Import, Empty, Copy, FromImage"
  default     = "Empty"
 }

variable "disk_size_gb" {
  description = "Specifies the size of the managed disk to create in gigabytes. If create_option is Copy or FromImage, then the value must be equal to or greater than the source's size."
  default     = "10"
 }
variable "disk_caching" {
  description = " Specifies the caching requirements for this Data Disk. Possible values include None, ReadOnly and ReadWrite"
  default     = "ReadWrite"
 }

 variable "lun_number" {
  description = "The Logical Unit Number of the Data Disk, which needs to be unique within the Virtual Machine"
  default     = "0"
 }

variable "vm_id" {
  description = "The ID of the Virtual Machine to which the Data Disk should be attached"
 }
variable "write_accelerator_enabled" {
  description = "Specifies if Write Accelerator is enabled on the disk. This can only be enabled on Premium_LRS managed disks with no caching and M-Series VMs."
  default     = false
 }


variable "location" {
  description = "Specified the supported Azure location where the resource exists"
 }

variable "resource_group_name" {
  description = "The name of the resource group in which to create the managed disk"
 }
variable "is_encryption_enabled" {
  description = "Is Encryption enabled on this Managed Disk"
  default     = true
 }

#variable "secret_url" {
 # description = "The URL to the Key Vault Secret used as the Disk Encryption Key."
 #}

#variable "source_vault_id" {
 # description = "The URL of the Key Vault."
 #}

variable "tags" {
  type    = map
  default = {}
 }
