resource "azurerm_managed_disk" "vmmd" {
  name                 = "${var.managed_disk_name}"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "${var.storage_account_type}"
  create_option        = "${var.create_option}"
  disk_size_gb         = "${var.disk_size_gb}"

  #encryption_settings {
    #enabled = "${is_encryption_enabled}"

    #disk_encryption_key {
     # secret_url      = "${var.secret_url}"
     # source_vault_id = "${var.source_vault_id}"
    #}
  #}

  tags = "${var.tags}"
}


resource "azurerm_virtual_machine_data_disk_attachment" "vmmd" {
  managed_disk_id           = "${azurerm_managed_disk.vmmd.id}"
  virtual_machine_id        = "${var.vm_id}"
  lun                       = "${var.lun_number}"
  caching                   = "${var.disk_caching}"
}
