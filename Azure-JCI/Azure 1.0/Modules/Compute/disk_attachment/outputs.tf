output "disk_attachment_id" {
  value = "${azurerm_virtual_machine_data_disk_attachment.vmmd.id}"
}

output "managed_disk_id" {
  value = "${azurerm_managed_disk.vmmd.id}"
}
