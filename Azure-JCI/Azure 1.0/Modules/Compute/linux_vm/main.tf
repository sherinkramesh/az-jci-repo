module "os" {
  source       = "./os"
  vm_os_simple = "${var.vm_os_simple}"
 }

resource "random_id" "vm-sa" {
  keepers = {
    vm_hostname = var.vm_hostname
   }

  byte_length = 6
 }


resource "azurerm_storage_account" "vm-sa" {
  count                    = "${var.boot_diagnostics == "true" ? 1 : 0}"
  name                     = "bootdiag${lower(random_id.vm-sa.hex)}"
  resource_group_name      = "${var.resource_group_name}"
  location                 = "${var.location}"
  account_tier             = "${element(split("_", var.boot_diagnostics_sa_type),0)}"
  account_replication_type = "${element(split("_", var.boot_diagnostics_sa_type),1)}"
  tags                     = "${var.tags}"
  }


resource "azurerm_virtual_machine" "vm-linux" {  
  name                          = "${var.vm_hostname}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  #availability_set_id           = "${azurerm_availability_set.vm.id}"
  vm_size                       = "${var.vm_size}"
  network_interface_ids         = ["${azurerm_network_interface.vm.id}"]  
  delete_os_disk_on_termination = "${var.delete_os_disk_on_termination}"

  storage_image_reference {
    id        = "${var.vm_os_id}"
    publisher = "${var.vm_os_id == "" ? coalesce(var.vm_os_publisher, module.os.calculated_value_os_publisher) : ""}"
    offer     = "${var.vm_os_id == "" ? coalesce(var.vm_os_offer, module.os.calculated_value_os_offer) : ""}"
    sku       = "${var.vm_os_id == "" ? coalesce(var.vm_os_sku, module.os.calculated_value_os_sku) : ""}"
    version   = "${var.vm_os_id == "" ? var.vm_os_version : ""}"
   }

  storage_os_disk {
    name              = "osdisk-${var.vm_hostname}"
    create_option     = "${var.create_option}"
    caching           = "${var.caching}"
    managed_disk_type = "${var.storage_account_type}"
  }

  os_profile {
    computer_name  = "${var.vm_hostname}"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.admin_username}/.ssh/authorized_keys"
      key_data = "${file("~/Downloads/Azure_TerraformScripts/sshkeys/publickey")}"
    }
  }
 
 

  tags = "${var.tags}"

  boot_diagnostics {
    enabled     = "${var.boot_diagnostics}"
    storage_uri = "${var.boot_diagnostics == "true" ? join(",", azurerm_storage_account.vm-sa.*.primary_blob_endpoint) : "" }"
  }
}

#resource "azurerm_availability_set" "vm" {
#  name                         = "${var.vm_hostname}-avset"
#  location                     = "${var.location}"
#  resource_group_name          = "${var.resource_group_name}"
#  platform_fault_domain_count  = "${var.fault_domain_count}"
#  platform_update_domain_count = "${var.update_domain_count}"
#  managed                      = true
#}

resource "azurerm_public_ip" "vm" {
  name                         = "${var.vm_hostname}-publicIP"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "${var.public_ip_address_allocation}"
}

resource "azurerm_network_interface" "vm" {
  name                      = "nic-${var.vm_hostname}"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  network_security_group_id = "${var.nsg_id}"

  ip_configuration {
    name                          = "ipconfig"
    subnet_id                     = "${var.vnet_subnet_id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.vm.id}"
  }
}
