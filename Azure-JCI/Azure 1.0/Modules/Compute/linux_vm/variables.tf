variable "vm_os_simple" {
  description = "Specify UbuntuServer, WindowsServer, RHEL, openSUSE-Leap, CentOS, Debian, CoreOS and SLES to get the latest image version of the specified os.  Do not provide this value if a custom value is used for vm_os_publisher, vm_os_offer, and vm_os_sku."
  default     = ""
 }
variable "vm_hostname" {
  description = "local name of the VM"
 }

variable "boot_diagnostics" {
  description = "(Optional) Enable or Disable boot diagnostics"
  default     = "false"
 }

variable "resource_group_name" {
  description = "The name of the resource group in which the resources will be created"
 }

variable "location" {
  description = "The location/region where the virtual network is created. Changing this forces a new resource to be created."
 }

variable "boot_diagnostics_sa_type" {
  description = "(Optional) Storage account type for boot diagnostics"
  default     = "Standard_LRS"
 }

variable "tags" {
  type        = map
  description = "A map of the tags to use on the resources that are deployed with this module."
  default = {}
 }
variable "vm_os_id" {
  description = "The resource ID of the image that you want to deploy if you are using a custom image.Note, need to provide is_Custom_image = true for windows custom images"
  default     = ""
 }
 variable "is_Custom_image" {
  description = "Boolean flag to notify when the custom image is windows based. Only used in conjunction with vm_os_id"
  default     = "false"
 }

variable "vm_os_offer" {
  description = "The name of the offer of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = ""
 }
variable "vm_size" {
  description = "Specifies the size of the virtual machine"
  default     = "Standard_B1S"
 }
variable "delete_os_disk_on_termination" {
  description = "Delete datadisk when machine is terminated"
  default     = "false"
 }
variable "vm_os_version" {
  description = "The version of the image that you want to deploy. This is ignored when vm_os_id or vm_os_simple are provided."
  default     = "latest"
 }
variable "create_option" {
  description = "Specifies how the OS Disk should be created. Possible values are Attach (managed disks only) and FromImage"
  default     = "FromImage"
 }

variable "caching" {
  description = "Specifies the caching requirements for the OS Disk. Possible values include None, ReadOnly and ReadWrite"
  default     = "ReadWrite"
 }
variable "storage_account_type" {
  description = "Defines the type of storage account to be created. Valid options are Standard_LRS, Standard_ZRS, Standard_GRS, Standard_RAGRS, Premium_LRS"
  default     = "Premium_LRS"
 }
variable "admin_username" {
  description = "The admin username of the VM that will be deployed"
  default     = "azureuser"
 }

variable "admin_password" {
  description = "The admin password to be used on the VMSS that will be deployed. The password must meet the complexity requirements of Azure"
  default     = ""
 }

variable "fault_domain_count" {
  description = "Specifies the number of fault domains that are used. Max 3 for now"
  default     = "2"
 }

variable "update_domain_count" {
  description = "Specifies the number of update domains that are used. Max 20 for now"
  default     = "2"
 }
variable "public_ip_address_allocation" {
  description = "Defines how an IP address is assigned. Options are Static or Dynamic."
  default     = "dynamic"
 }


variable "nsg_id" {
  description = "NSG IF to be attached with NIC"
  default     = ""
 }
variable "vnet_subnet_id" {
  description = "The subnet id of the virtual network where the virtual machines will reside"
  default = ""
 }

variable "vm_os_publisher" {
  default     = ""
 }
variable "vm_os_sku" {
  default     = ""
 }