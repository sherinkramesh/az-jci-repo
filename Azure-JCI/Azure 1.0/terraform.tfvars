#resource_group values
resource_group_name = "JCI-RG"
location ="eastus"
tags = {
    environment = "Dev"
    costcenter = "it"
 }

#nsg values
nsg_name ="web-server-nsg"

#nsg_rules values
rule_name = "RDP Inbound"
rule_priority= 100
rule_direction ="Inbound"
destination_port_range= "3389"
#destination_port_range= "22"
rule_description = " This Rule is created to enable the RDP port number"

#virtual network  values
vnet_name = "AZJCI-Vnet"
address_space = ["10.25.0.0/16" ]

#subnet values
#subnet_prefixes = ["10.25.1.0/24","10.25.2.0/24","10.25.3.0/24"]
#subnet_names = ["Frontend","Firewalls","Backend"]
subnet_prefixes = ["10.25.1.0/24"]
subnet_names = ["Frontend-Subnet"]
#Virtual machine values
#vm_os_simple = "RHEL"
vm_os_simple = "WindowsServer"
boot_diagnostics = "false"
boot_diagnostics_sa_type = "Standard_LRS"
vm_hostname = "vm-porta1"
vm_size = "Standard_B2ms"
delete_os_disk_on_termination = "false"
create_option = "FromImage"
caching = "ReadWrite"
storage_account_type = "Premium_LRS"
admin_username = "Webserveradmin"
admin_password = "Password123!"
#admin_password = "C:\\Azure_TerraformScripts\\sshkeys\\publickey"
fault_domain_count = "2"
update_domain_count = "2"

#disk atatchment values 
managed_disk_name = "vm-porta1-disk1"
disk_storage_account_type = "Standard_LRS"
disk_create_option = "Empty"
disk_size_gb = "10"
lun_number = "10"
disk_caching = "ReadWrite"


#Backup configuration values
recovery_service_name = "test-backup-recovery"

#metric alert values
metric_rule_name = "vmutilization"
metric_name = "Percentage CPU"
metric_operator = "GreaterThan"
threshold = 50
aggregation = "Total"
custom_emails = "mohammed.abdulhammed@gmail.com"
action_group_name = "vmcpualert"
alert_description = "Action will be triggered when CPU percentage  is greater than 50."

#Route Table values
 route_table_name = "BackendUDR"
 route_name = "Backendroutes"
 next_hop_type = "VnetLocal"
 #next_hop_in_ip_address = "10.0.1.0"